%
% $Id: gvarda_utils.pl 99 2009-09-27 12:28:34Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_utils.pl 99 2009-09-27 12:28:34Z jar0s $'),nl.

get_ard_root(Property) :-
	ard_property(Property),
	\+ard_hist(Property, _),
	\+ard_depend(Property, _).

create_att_from_object(Object) :-
	get(Object, key, Attribute),
	ard_att_add(Attribute).

att_exists(DictItem) :-
	get(DictItem, key, Att),
	\+ ard_att(Att).

add_att_conditional(Att) :-
	\+ ard_att(Att),
	ard_att_add(Att).

add_att_conditional(_).

rewrite_logs(_, _, Start, Stop) :-
	Start >= Stop.

rewrite_logs(Old, New, Start, Stop) :-
	Start < Stop,
	get(Old, element, Start, Element),
	send(New, append, Element),
	Tmp is Start + 1,
	rewrite_logs(Old, New, Tmp, Stop).

property_exists(Node) :-
	get(Node, property, Property),
	\+ ard_property(Property).

rollback_att_add(Att) :-
	ard_att(Att),
	retract(ard_att(Att)).

create_list_from_dict(Chain, Old, New) :-
	send(Chain, empty),
	New = Old.

create_list_from_dict(Chain, Old, New) :-
	\+send(Chain, empty),
	get(Chain, tail, Dict_item),
	get(Dict_item, key, Att),
	ard_att_add(Att),
	Temp = [Att|Old],
	send(Chain, delete_tail),
	create_list_from_dict(Chain, Temp, New).

chain2l(Chain, List) :-
	chain2l(Chain, [], List).

chain2l(Chain, Old, New) :-
	send(Chain, empty),
	New = Old.

chain2l(Chain, Old, New) :-
	\+send(Chain, empty),
	get(Chain, tail, PreviewNode),
	get(PreviewNode, property, Property),
	create_list_from_code_vector(Property, PList),
	Temp = [PList|Old],
	send(Chain, delete_tail),
	chain2l(Chain, Temp, New).

lb2l(Lb, New) :-
	lb2l(Lb, [], New).

lb2l(Lb, Old, New) :-
	send(Lb?selection, empty),
	New = Old.

lb2l(Lb, Old, New) :-
	\+send(Lb?selection, empty),
	get(Lb?selection, tail, Dict_item),
	get(Dict_item, key, Att),
	Temp = [Att|Old],
	send(Lb?dict, delete, Dict_item),
	lb2l(Lb, Temp, New).

create_list_from_code_vector(CodeVector, List) :-
	get(CodeVector, low_index, From),
	get(CodeVector, high_index, To),
	cv2l(CodeVector, From, To, [], List).

cv2l(_, From, To, Old, New) :-	
	From > To,
	New = Old.

cv2l(CodeVector, From, To, Old, New) :-
	From =< To,
	get(CodeVector, element, To, Element),
	Temp = [Element|Old],
	Next is To - 1,
	cv2l(CodeVector, From, Next, Temp, New).

convert_arg(CodeVector, List) :-
	get(CodeVector, low_index, From),
	get(CodeVector, high_index, To),
	cv2l_recursive(CodeVector, From, To, [], List).

cv2l_recursive(_, From, To, Old, New) :-
	From > To,
	New = Old.

cv2l_recursive(CodeVector, From, To, Old, New) :-
	From =< To,
	get(CodeVector, element, To, Element),
	( send(Element, instance_of, code_vector)
	-> convert_arg(Element, EList),
		Temp = [EList|Old]
	; Temp = [Element|Old]
	),
	Next is To - 1,
	cv2l_recursive(CodeVector, From, Next, Temp, New).

extract_relations(Chain, Old, New) :-
	send(Chain, empty),
	New = Old.

extract_relations(Chain, Old, New) :-
	\+send(Chain, empty),
	get(Chain, head, PreviewNode),
	(get(PreviewNode, connections, Connections)
	-> 	new(CopiedConnections, chain),
		send(Connections, for_all, message(CopiedConnections, append, @arg1)),
		send(CopiedConnections, unique),
		analyze_connections(PreviewNode, CopiedConnections, Old, Tmp)
	; 	analyze_connections(PreviewNode, new(_, chain), Old, Tmp)
	),
	send(Chain, delete_head),
	extract_relations(Chain, Tmp, New).

analyze_connections(_, Chain, Old, New) :-
	send(Chain, empty),
	New = Old.
	
analyze_connections(PreviewNode, Chain, Old, New) :-
	\+send(Chain, empty),
	get(Chain, head, Connection),
	get(Connection, from, FromNode),
	get(Connection, to, ToNode),
	( \+both_new_nodes(PreviewNode, FromNode, ToNode)
	-> 	get(FromNode, property, FromProperty),
		get(ToNode, property, ToProperty),
		create_list_from_code_vector(FromProperty, FromList),
		create_list_from_code_vector(ToProperty, ToList),
		( \+list_contains([FromList, ToList], Old)
		->	Tmp = [[FromList, ToList]|Old]
		;	Tmp = Old
		)
	; 	Tmp = Old
	),
	send(Chain, delete_head),
	analyze_connections(PreviewNode, Chain, Tmp, New).

both_new_nodes(PreviewNode, FromNode, ToNode) :-
	FromNode \== ToNode,	
	get(FromNode, editable, FromFlag),
	FromFlag = @on,
	get(ToNode, editable, ToFlag),
	ToFlag = @on,
	PreviewNode == ToNode.

gvarda_undo(Property) :-
	create_list_from_code_vector(Property, PList),
	forall(ard_hist(PList, X), 
		(
		gvarda_undo_helper(PList, X), 		
		gvarda_undo_helper_clear(PList, X)
		)).

gvarda_undo_helper(NewProperty,CollapsedProperty):-
	ard_depend(From,CollapsedProperty),
	\+ ard_hist(NewProperty,From),
	ard_depend_add(From,NewProperty),
	fail.

gvarda_undo_helper(NewProperty,CollapsedProperty):-
	ard_depend(CollapsedProperty,To),
	\+ ard_hist(NewProperty,To),
	ard_depend_add(NewProperty,To),
	fail.

gvarda_undo_helper(_,_).

gvarda_undo_helper_clear(F,CF):-
	ard_depend_del_all(CF),
	ard_property_del(CF),
	ard_hist_del(F,CF),
	remove_property_att(CF).

remove_property_att([]).

remove_property_att(List) :-
	[Att|Tail] = List,
	\+ ard_property_exists(Att),
	retract(ard_att(Att)),
	remove_property_att(Tail).

remove_property_att([_|Tail]) :-
	remove_property_att(Tail).

ard_property_exists(Att) :-
	ard_property(P),
	list_contains(P, Att).

list_contains([E|_], E).
list_contains([_|T], E) :-
	list_contains(T, E).
