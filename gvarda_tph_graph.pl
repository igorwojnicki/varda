%
% $Id: gvarda_tph_graph.pl 101 2009-09-27 14:55:38Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_tph_graph.pl 101 2009-09-27 14:55:38Z jar0s $'),nl.

:- require([ ard_property/1
	   , ard_hist/2
	   , concat_atom/3
	   ]).

:- pce_begin_class(tph_graph, picture).

variable(tph_tree, tree, get, "Tree that will be used to draw TPH graph.").

initialise(TPHgraph, Width:int, Height:int) :->
	"Create tph_graph"::
	send(TPHgraph, send_super, initialise, 'TPH', size(Width, Height)),
	send(TPHgraph, slot, tph_tree, new(_, tree)),
	send(TPHgraph, recogniser, click_gesture(left, '', single,
				message(TPHgraph, generate))).

:- pce_group(internal).

clear(P) :->
	"Clear the diagram"::
	send(P, send_super, clear).

check_selection(_, Node:node, Name:name) :->
	"Check whether Node should be selected and sets proper colour."::
	get(Node, name, NName),
	( NName == Name
	-> send(Node, colour, red)
	; send(Node, colour, black)
	).

make_tree(Tph, RootNode:node) :->
	"Creates new tree with the RootNode and configures it."::
	new(Tree, tree(RootNode)),
	send(Tph, slot, tph_tree, Tree),
	
	send(Tree, neighbour_gap, 10),
	send(Tree?link, arrows, second),
	send(Tree, link_gap, 0),
	send(Tree, direction, vertical),
	send(Tree, alignment, center),
	send(Tree, border, 5),
	send(Tree, zoom, RootNode).

get_tree_root(_, N) :<-
	"Returns ARD property which is the root of the TPH tree."::
	ard_property(N),
	\+ard_hist(_, N).

:- pce_group(public).

generate(Tph) :->
	"Generate TPH diagram"::
	send(Tph, clear),
	get(Tph, get_tree_root, RootElement),
	create_node(RootElement, RootNode),
	send(Tph, make_tree, RootNode),
	get(Tph, tph_tree, Tree),
	send(Tph, display, Tree, point(5,5)),
	forall(ard_hist(RootElement, X),
		(
		draw_children(X, RootNode)
		)).

select_node(P, Name:name) :->
	"Set node with given name as selected"::
	get(P, tph_tree, Tree),
	send(Tree, for_all, message(P, check_selection, @arg1, Name)).

:- pce_end_class.

draw_children(Child, ParentNode) :-
	create_node(Child, ChildNode),
	send(ParentNode, son, ChildNode),
	forall(ard_hist(Child, NextChild),
		(
		draw_children(NextChild, ChildNode)
		)).

create_node(Text, Node) :-
	concat_atom(Text, '\n', Txt),
	new(Textg, text(Txt)),
	send(Textg, format, center),
	new(Figure, figure),
	send(Figure, name, Txt),
	send(Figure, display, Textg),
	send(Figure, border, 5),
	send(Figure, pen, 2),
	send(Figure, alignment, center),
	new(Node, node(Figure)),
	send(Node, name, Txt).

