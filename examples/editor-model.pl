%
% $Id$
%
% 


editor_model:-
	ard_att_add('Editor'),
	ard_property_add(['Editor']),

	ard_finalize_att_add(['Editor'],
			     ['Text','Operations']),

	ard_split(['Text','Operations'],
		  [['Text'],['Operations']],
		  [[['Text'],['Operations']],
		   [['Operations'],['Text']]
		   ]),

	ard_finalize_att_add(['Text'],
			     [chars,cursor]),

	ard_split([chars,cursor],
		  [[chars],[cursor]],
		  [
		   [[cursor],[chars]]
		   ]),

	
	ard_finalize_att_add(['Operations'],
			     ['Open','Save','Keyboard']),

	ard_split(['Open','Save','Keyboard'],
		  [['Open'],['Save'],['Keyboard']],
		  [
		   [['Open'],[chars]],

		   [[chars],['Save']],
		   [['Keyboard'],[chars]],
		   [['Keyboard'],[cursor]]
		   ]),

	ard_finalize_att_add(['Open'],
			     [inchar,ichar]),

	ard_finalize_att_add(['Save'],
			     [outchar,ochar]),
	
	ard_finalize_att_add(['Keyboard'],
			     [inkey,key]),

	ard_split([inkey,key],
		  [[inkey],[key]],
		  [
		   [[inkey],[key]],
		   [[key],[chars]],
		   [[key],[cursor]]
		   ]),
	
% observation: never use dependencies to express non-functional dependencies i.e. control!!! it is added in the XTT !!!!
	

	ard_split([inchar,ichar],
		  [[inchar],[ichar]],
		  [
		   [[inchar],[ichar]],
		   [[ichar],[chars]]
		   ]),

	ard_split([outchar,ochar],
		  [[outchar],[ochar]],
		  [
		   [[ochar],[outchar]],
		   [[chars],[ochar]]
		   ]).

	
:-editor_model.
					       

