%
% $Id: elevator-model.pl,v 1.7 2008-03-06 12:34:36 wojnicki Exp $
%
% Elevator, 
%
% source:
% http://www.win.tue.nl/~mchaudro/sa2006/elevator%20example%20in%20UML.pdf
% 
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


elevator_model:-
	ard_att_add('Elevator'),
	ard_property_add(['Elevator']),
	
% use cases, identified attribs
	ard_att_add('CarCalls'),
	ard_att_add('MoveStopCar'),
	ard_att_add('OpenCloseDoors'),
	ard_att_add('IndicateMovingDirection'),
	ard_att_add('HallCalls'),
	ard_att_add('IndicateCarPosition'),
	ard_att_add('EmergencyBrake'),
% -----------

% structurizing the above:
        ard_att_add('Calls'),
	ard_att_add('Indicator'),
	ard_att_add('Operation'),
% feedback with environment
	ard_att_add('Position'),

	ard_finalize(['Elevator'],
		     ['Calls','Indicator','Operation','Position']),
	
	ard_split(['Calls','Indicator','Operation','Position'],
		  [['Calls'],['Indicator'],['Operation'],['Position']],
		  [
		   [['Calls'],['Operation']],
		   [['Operation'],['Indicator']],
		   [['Position'],['Operation']],
		   [['Position'],['Indicator']]
		  ]),

	ard_finalize(['Calls'],
		     ['CarCalls','HallCalls','EmergencyBrake']),

	ard_finalize(['Indicator'],
		     ['IndicateMovingDirection','IndicateCarPosition']),

	ard_att_add('Scheduler'),

	ard_finalize(['Operation'],
		     ['OpenCloseDoors','MoveStopCar','Scheduler']),


	ard_split(['CarCalls','HallCalls','EmergencyBrake'],
		  [['CarCalls'],['HallCalls'],['EmergencyBrake']],
		  [
		   [['CarCalls'],['OpenCloseDoors','MoveStopCar','Scheduler']],
		   [['HallCalls'],['OpenCloseDoors','MoveStopCar','Scheduler']],
		   [['EmergencyBrake'],['OpenCloseDoors','MoveStopCar','Scheduler']]
		  ]),

	ard_split(['OpenCloseDoors','MoveStopCar','Scheduler'],
		  [['OpenCloseDoors'],['MoveStopCar'],['Scheduler']],
		  [
		   [['Scheduler'],['MoveStopCar']],
		   [['MoveStopCar'],['OpenCloseDoors']],
		   [['CarCalls'],['Scheduler']],
		   [['HallCalls'],['Scheduler']],
		   [['EmergencyBrake'],['MoveStopCar']],
		   [['MoveStopCar'],['IndicateMovingDirection','IndicateCarPosition']],
		   [['Position'],['Scheduler']],
		   [['Position'],['MoveStopCar']],
		   [['OpenCloseDoors'],['MoveStopCar']]
		  ]),

% refactoring, adding Position to the early stage, since there is no working predicate doing that :(
	
	ard_split(['IndicateMovingDirection','IndicateCarPosition'],
		  [['IndicateMovingDirection'],['IndicateCarPosition']],
		  [[['MoveStopCar'],['IndicateMovingDirection']],
		   [['Position'],['IndicateCarPosition']]
		   ]),

% refactoring

	ard_depend_add(['Position'],['OpenCloseDoors']),

	ard_finalize_att_add(['Position'],[position]),

	ard_finalize_att_add(['IndicateCarPosition'],[positionIndicator]),

 	ard_finalize_att_add(['CarCalls'],
 		     [carCall0,carCall1,carCall2,carCall3]),

	ard_finalize_att_add(['HallCalls'],
		     [hallCall0,hallCall1,hallCall2,hallCall3]),

	ard_split([hallCall0,hallCall1,hallCall2,hallCall3],
		  [[hallCall0],[hallCall1],[hallCall2],[hallCall3]],
		  [[[hallCall0],['Scheduler']],
		   [[hallCall1],['Scheduler']],
		   [[hallCall2],['Scheduler']],
		   [[hallCall3],['Scheduler']]
		   ]),

	ard_split([carCall0,carCall1,carCall2,carCall3],
		  [[carCall0],[carCall1],[carCall2],[carCall3]],
		  [[[carCall0],['Scheduler']],
		   [[carCall1],['Scheduler']],
		   [[carCall2],['Scheduler']],
		   [[carCall3],['Scheduler']]
		   ]),

	ard_finalize_att_add(['EmergencyBrake'],[emergencyBrake]),

	ard_finalize_att_add(['OpenCloseDoors'],
			     [openCarDoor,carDoorClosed,
			      openDoor0,doorClosed0,
			      openDoor1,doorClosed1,
			      openDoor2,doorClosed2,
			      openDoor3,doorClosed3
			     ]),
	
	ard_split([openCarDoor,carDoorClosed,
		   openDoor0,doorClosed0,
	           openDoor1,doorClosed1,
		   openDoor2,doorClosed2,
		   openDoor3,doorClosed3],
		   [[openCarDoor],[carDoorClosed],
		   [openDoor0],[doorClosed0],
		   [openDoor1],[doorClosed1],
		   [openDoor2],[doorClosed2],
		   [openDoor3],[doorClosed3]],
		   [
		    [[position],[openCarDoor]],
		    [[position],[openDoor0]],
		    [[position],[openDoor1]],
		    [[position],[openDoor2]],
		    [[position],[openDoor3]],
		    [[carDoorClosed],['MoveStopCar']],
		    [[doorClosed0],['MoveStopCar']],
		    [[doorClosed1],['MoveStopCar']],
		    [[doorClosed2],['MoveStopCar']],
		    [[doorClosed3],['MoveStopCar']],
		    [['MoveStopCar'],[openCarDoor]],
		    [['MoveStopCar'],[openDoor0]],
		    [['MoveStopCar'],[openDoor1]],
		    [['MoveStopCar'],[openDoor2]],
		    [['MoveStopCar'],[openDoor3]]
		   ]),

 	ard_finalize_att_add(['MoveStopCar'],[move]),

	ard_finalize_att_add(['IndicateMovingDirection'],
			     [goingUp,goingDown]),
	ard_split([goingUp,goingDown],
		  [[goingUp],[goingDown]],
		  [[[move],[goingUp]],
		   [[move],[goingDown]]
		  ]),

	ard_finalize_att_add(['Scheduler'],[whereToMove]),

%refactoring

	ard_depend_add([doorClosed0],[hallCall0]),
	ard_depend_add([doorClosed1],[hallCall1]),
	ard_depend_add([doorClosed2],[hallCall2]),
	ard_depend_add([doorClosed3],[hallCall3]),
	
	ard_depend_add([doorClosed0],[carCall0]),
	ard_depend_add([doorClosed1],[carCall1]),
	ard_depend_add([doorClosed2],[carCall2]),
	ard_depend_add([doorClosed3],[carCall3]),

	ard_depend_add([position],[hallCall0]),
	ard_depend_add([position],[hallCall1]),
	ard_depend_add([position],[hallCall2]),
	ard_depend_add([position],[hallCall3]),

	ard_depend_add([position],[carCall0]),
	ard_depend_add([position],[carCall1]),
	ard_depend_add([position],[carCall2]),
	ard_depend_add([position],[carCall3])
	
	.

:-elevator_model.
					       

