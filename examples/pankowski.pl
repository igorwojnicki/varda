pankowski :-
	ard_att_add('Firma'),
	ard_property_add(['Firma']),
	
	ard_att_add('Osoby'),
	ard_att_add('Lokal'),
	
	ard_finalize(['Firma'],['Osoby','Lokal']),
	
	ard_split(['Osoby','Lokal'],[['Osoby'],['Lokal']],[[['Osoby'],['Lokal']]]),
	
	ard_att_add(kierownik),
	ard_att_add(pracownik),

	ard_att_add(pokoj),
	ard_att_add(budynek),
	ard_att_add(powierzchnia),
	
	ard_finalize(['Osoby'],[kierownik,pracownik]),
	ard_finalize(['Lokal'],[pokoj,budynek,powierzchnia]).

