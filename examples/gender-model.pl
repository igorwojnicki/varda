gm :-
	ard_att_add('GenderMachine'),
	ard_property_add(['GenderMachine']),
	
	ard_att_add('Input'),
	ard_att_add('Processing'),
	ard_att_add('Output'),
	ard_finalize(['GenderMachine'],['Input','Output','Processing']),
	ard_split(['Input','Output','Processing'],
	          [['Input'],['Output'],['Processing']],
		  [
                   [['Input'],['Processing']],
                   [['Processing'],['Output']]
	          ]),
	
	ard_att_add('InFName'),
	ard_att_add('InLName'),

	ard_att_add('OutFName'),
	ard_att_add('OutLName'),
	ard_att_add('OutGender'),

	ard_att_add('FName'),
	ard_att_add('LName'),
	ard_att_add('Gender'),
	ard_att_add('FemaleName'),

 	ard_finalize(['Input'],['InFName','InLName']),
 	ard_finalize(['Output'],['OutFName','OutLName','OutGender']),	
 	ard_finalize(['Processing'],['FemaleName','FName','LName','Gender']),
	
	ard_split(['InFName','InLName'],
	          [['InFName'],['InLName']],
		  [
	           [['InLName'],['FemaleName','FName','LName','Gender']],
	           [['InFName'],['FemaleName','FName','LName','Gender']]
	          ]),

	ard_split(['OutFName','OutLName','OutGender'],
	          [['OutFName'],['OutLName'],['OutGender']],
		  [
	           [['FemaleName','FName','LName','Gender'],['OutLName']],
	           [['FemaleName','FName','LName','Gender'],['OutFName']],
	           [['FemaleName','FName','LName','Gender'],['OutGender']]
	          ]),

	ard_split(['FemaleName','FName','LName','Gender'],
                  [['FemaleName'],['FName'],['LName'],['Gender']],
		  [
	           [['FemaleName'],['Gender']],
	           [['FName'],['Gender']],
	           [['InFName'],['FName']],
	           [['InFName'],['LName']],
 	           [['InLName'],['LName']],
 	           [['InLName'],['FName']],
  	           [['LName'],['OutLName']],
  	           [['FName'],['OutLName']],
  	           [['FName'],['OutFName']],
  	           [['LName'],['OutFName']],
	           [['Gender'],['OutFName']],
	           [['Gender'],['OutLName']],
	           [['FName'],['OutGender']],
	           [['LName'],['OutGender']],
	           [['Gender'],['OutGender']]

	          ]),

	ard_att_add(infname),
	ard_att_add(inlname),
	ard_att_add(lname),
	ard_att_add(fname),
	ard_att_add(gender),
	ard_att_add(femalename),
	ard_att_add(outgender),
	ard_att_add(outfname),
	ard_att_add(outlname),
	
	ard_finalize(['InFName'],['infname']),
	ard_finalize(['InLName'],['inlname']),
	ard_finalize(['LName'],['lname']),
	ard_finalize(['FName'],['fname']),
	ard_finalize(['Gender'],['gender']),
	ard_finalize(['FemaleName'],['femalename']),
	ard_finalize(['OutGender'],['outgender']),
	ard_finalize(['OutFName'],['outfname']),
	ard_finalize(['OutLName'],['outlname']).
	


%%  	ard_split(['FemaleName','FName','LName','Gender'],
%%  		  [['FemaleName'],['FName'],['LName'],['Gender']],
%%  		  [
%%  		   [['FName'],['Gender']],
%%  		   [['FemaleName'],['Gender']],
%%  		   [['FName'],['LName']],
%%  		   [['LName'],['FName']]
%%  		  ]).



:-gm.