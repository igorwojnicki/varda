:- dynamic ard_att/1.

ard_att('Pankowski').

ard_att(pracownik).
ard_att(kierownik).
ard_att(pokoj).
ard_att(budynek).
ard_att(powierzchnia).

:- dynamic ard_property/1.

ard_att(['Bernstein']).
ard_property([pracownik]).
ard_property([kierownik]).
ard_property([pokoj]).
ard_property([budynek]).
ard_property([powierzchnia]).

:- dynamic ard_depend/2.

ard_depend([pracownik], [kierownik]).
ard_depend([pracownik], [pokoj]).
ard_depend([kierownik], [pokoj]).
ard_depend([pokoj], [budynek]).
ard_depend([pokoj], [powierzchnia]).

:- dynamic ard_hist/2.

ard_hist(['Pankowski'],[pracownik]).
ard_hist(['Pankowski'],[kierownik]).
ard_hist(['Pankowski'],[pokoj]).
ard_hist(['Pankowski'],[budynek]).
ard_hist(['Pankowski'],[powierzchnia]).

