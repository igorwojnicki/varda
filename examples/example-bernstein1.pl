:- dynamic ard_att/1.

ard_att('Bernstein').

ard_att(a).
ard_att(b).
ard_att(c).
ard_att(d).
ard_att(e).
ard_att(f).

:- dynamic ard_property/1.

ard_att(['Bernstein']).
ard_property([a]).
ard_property([b]).
ard_property([c]).
ard_property([d]).
ard_property([e]).
ard_property([f]).

:- dynamic ard_depend/2.

ard_depend([a], [b]).
ard_depend([a], [c]).
ard_depend([b], [c]).
ard_depend([b], [d]).
ard_depend([d], [b]).
ard_depend([a], [f]).
ard_depend([b], [f]).
ard_depend([e], [f]).

:- dynamic ard_hist/2.

ard_hist(['Bernstein'],[a]).
ard_hist(['Bernstein'],[b]).
ard_hist(['Bernstein'],[c]).
ard_hist(['Bernstein'],[d]).
ard_hist(['Bernstein'],[e]).
ard_hist(['Bernstein'],[f]).
