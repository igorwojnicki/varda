%
% $Id$
%
% 


snake_model(0):-
	ard_att_add('SnakeGame'),
	ard_property_add(['SnakeGame']),

	ard_finalize_att_add(['SnakeGame'],
			     ['Snake','Control','Apple','Wall']),

	ard_split(['Snake','Control','Apple','Wall'],
		  [['Control'],['Snake'],['Apple'],['Wall']],
		  [[['Snake'],['Apple']],
		   [['Apple'],['Snake']],
		   [['Wall'],['Snake']],
		   [['Control'],['Snake']]
		  ]).

snake_model(1):-
	ard_finalize_att_add(['Snake'],
		     [length,direction,'Head','Turn']),
	ard_split([length,direction,'Head','Turn'],
		  [[length],[direction],['Head'],['Turn']],
		  [
		   [['Head'],['Apple']],
		   [['Apple'],[length]],
		   [['Wall'],['Head']],
		   [['Control'],[direction]],
		   [['Control'],['Turn']],
		   [[],[length]]

		  ]),!.
	
snake_model:-snake_model(X), write(X), nl, fail.
snake_model.
					       

% 

split_assist(Property):-
	ard_depend(Property,X), write('['), write(Property), write(','), write(X), write(']'), nl, fail.
split_assist(Property):-
	ard_depend(X,Property), write('['), write(X), write(','), write(Property), write(']'), nl, fail.
split_assist(_).





