%
% $Id: gvarda_split_preview.pl 99 2009-09-27 12:28:34Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_split_preview.pl 99 2009-09-27 12:28:34Z jar0s $'),nl.

:- pce_begin_class(split_preview, picture).

variable(predecessor_nodes, chain, get, "List of all nodes that are predecessor to the one that is being split..").
variable(consequent_nodes, chain, get, "List of all nodes that are consequent from the one that is being split.").
variable(new_nodes, chain, get, "List of newly created graph nodes").
variable(property, code_vector, get, "Property that is being split.").
variable(px, int, get, "X coordinate of next predecessor node will be placed at.").
variable(py, int, get, "Y coordinate of next predecessor node will be placed at.").
variable(cx, int, get, "X coordinate of next consequent node will be placed at.").
variable(cy, int, get, "Y coordinate of next consequent node will be placed at.").
variable(nx, int, get, "X coordinate of next new node will be placed at.").
variable(ny, int, get, "Y coordinate of next new node will be placed at.").
variable(selected_node, split_preview_node*, get, "Currently selected node.").
variable(parent, split_window, get, "Parent object.").

initialise(Sp, Parent:split_window, Property:code_vector, Width:int, Height:int) :->
	"Create ard property split preview"::
	send(Sp, send_super, initialise, 'Split preview', size(Width, Height)),
	send(Sp, slot, parent, Parent),	
	send(Sp, slot, predecessor_nodes, new(_, chain)),
	send(Sp, slot, consequent_nodes, new(_, chain)),
	send(Sp, slot, new_nodes, new(_, chain)),
	send(Sp, slot, property, Property),
	send(Sp, label, 'Relations preview:'),
	send(Sp, border, 1),
	send(Sp, pen, 1),	
	send(Sp, recogniser, 
		click_gesture(left, '', single, message(Sp, single_clicked))
	),
	send(Sp, generate_related_nodes).

:- pce_group(internal).

clear(P) :->
	"Clear the diagram"::
	send(P, send_super, clear).

generate_related_nodes(P) :->
	"Create nodes that are related to/from the property which is being split."::
	send(P, clear),
	get(P, property, Property),
	create_list_from_code_vector(Property, PList),

	get(P, visible, area(X, Y, W, _)),
	send(P, slot, px, X + W - 5),
	send(P, slot, py, Y + 5),
	send(P, slot, cx, X + 5),
	send(P, slot, cy, Y + 5),
	send(P, slot, nx, X + W / 2),
	send(P, slot, ny, Y + 5),

	forall(ard_depend(From, PList),
	   (
	   concat_atom(From, '\n', FromText),
	   send(P, add_predecessor_node, FromText, From)	   )),

	forall(ard_depend(PList, To),
	   (
	   concat_atom(To, '\n', ToText),
	   send(P, add_consequent_node, ToText, To)
	   )).

order_nodes(P) :->
	"Order new nodes. Called when one of new nodes is removed."::
	get(P, visible, area(X, Y, W, _)),
	send(P, slot, nx, X + W / 2),
	send(P, slot, ny, Y + 5),
	send(P?new_nodes, for_all, message(P, position_node, @arg1)).

position_node(P, Node:split_preview_node) :->
	"Place given node just under the previous one"::
	get(P, nx, Nx),
	get(P, ny, Ny),
	send(Node, position, point(Nx - Node?width/2, Ny)),
	send(P, slot, ny, Ny + Node?height + 20).

add_predecessor_node(P, Name:name, Property:code_vector) :->
	"Create new node and add it to predecessor nodes list."::
	get(P?property, copy, CurrentP),
	create_list_from_code_vector(CurrentP, CurrentList),
	get(Property, copy, PropertyC),
	create_list_from_code_vector(PropertyC, PropertyList),
	(\+ CurrentList = PropertyList
	->	new(Node, split_preview_node(Name, Property, P, false)),
		get(P, predecessor_nodes, PNodes),
		send(PNodes, add, Node),
		send(P, slot, predecessor_nodes, PNodes),
		get(P, px, Px),
		get(P, py, Py),	
		send(P, display, Node, point(Px - Node?width, Py)),
		send(P, slot, py, Py + Node?height + 10),
		send(Node, handle, @in_handle)
	;	true
	).

add_consequent_node(P, Name:name, Property:code_vector) :->
	"Create new node and add it to the consequent nodes list."::
	get(P?property, copy, CurrentP),
	create_list_from_code_vector(CurrentP, CurrentList),
	get(Property, copy, PropertyC),
	create_list_from_code_vector(PropertyC, PropertyList),
	( \+ CurrentList = PropertyList
	->	new(Node, split_preview_node(Name, Property, P, false)),
		get(P, consequent_nodes, CNodes),
		send(CNodes, add, Node),
		send(P, slot, consequent_nodes, CNodes),
		get(P, cx, Cx),
		get(P, cy, Cy),
		send(P, display, Node, point(Cx, Cy)),
		send(P, slot, cy, Cy + Node?height + 10),
		send(Node, handle, @out_handle)
	; 	true
	).

:- pce_group(public).

add_new_node(P, Name:name, Property:code_vector) :->
	"Create new node and add it to the new nodes list."::
	new(Node, split_preview_node(Name, Property, P, true)),	
	get(P, new_nodes, CNodes),
	send(CNodes, add, Node),
	send(P, slot, new_nodes, CNodes),
	get(P, nx, Nx),
	get(P, ny, Ny),
	send(P, display, Node, point(Nx - Node?width/2, Ny)),
	send(P, slot, ny, Ny + Node?height + 20),
	send_list(Node, handle, [@in_handle, @out_handle, @middle_handle]).
	

single_clicked(P) :->
	"Deselect currently selected node. \nCalled when the user clicks on the preview outside of the nodes."::
	get(P, new_nodes, Nodes),
	send(P, slot, selected_node, @nil),
	send(Nodes, for_all, message(@arg1, colour, black)).

clear_relations(P) :->
	"Remove all relations between nodes"::
	get(P, new_nodes, NNodes),
	get(P, consequent_nodes, CNodes),
	get(P, predecessor_nodes, PNodes),
	send(NNodes, for_all, message(@arg1, remove_relations)),
	send(CNodes, for_all, message(@arg1, remove_relations)),
	send(PNodes, for_all, message(@arg1, remove_relations)).

:- pce_end_class.

:- pce_begin_class(split_preview_node(name, chain, split_preview), figure).

handle(w/2, 0, link, link).

variable(property, code_vector, get, "Property that particular node represents.").
variable(parent, split_preview, get, "Graph that particular node belogns to").
variable(editable, bool, get, "Flag to indicate either node can be removed from graph or not.").

%define global handles and links so that the user can connect the nodes.
:- pce_global(@in_handle, new(handle(0, h/2, out, out))).
:- pce_global(@out_handle, new(handle(w, h/2, in, in))).
:- pce_global(@middle_handle, new(handle(w/2, 0, middle, middle))).
:- pce_global(@inout_link, new(link(out, in, line(arrows := second)))).
:- pce_global(@self_link, new(link(out, middle, line(arrows := second)))).

initialise(Node, Name:name, Property, Parent:split_preview, Editable:bool) :->
	"Create new graph node"::
	send(Node, send_super, initialise),
	send(Node, display, text(Name, center)),
	send(Node, border, 5),
	send(Node, pen, 2),
	send(Node, alignment, center),
	send(Node, send_super, name, Name),
	send(Node, slot, property, Property),
	send(Node, slot, parent, Parent),
	send(Node, slot, editable, Editable).

:- pce_group(internal).

name(Node, Name:name) :->
	"Change name of a node"::
	get(Node, member, text, Text),
	send(Text, string, Name),
	send(Node, send_super, name, Name).

%define global recognisers and append different actions to them
:- pce_global(@non_editable_recogniser, make_non_editable_recogniser).
:- pce_global(@editable_recogniser, make_editable_recogniser).

make_non_editable_recogniser(R) :-	
	new(R, handler_group(
				click_gesture(left, 'c', single, message(@receiver, remove_relation)),
				connect_gesture(left, 'c', @inout_link),
				move_gesture(left)
			)).

make_editable_recogniser(R) :-	
	new(R, handler_group(
				click_gesture(left, 'c', single, message(@receiver, remove_relation)),
				connect_gesture(left, 'c', @inout_link),
				click_gesture(left, '', double, message(@receiver, double_clicked)),
				click_gesture(left, '', single, message(@receiver, single_clicked)),		
				move_gesture(left)
			)).

event(Node, Ev:event) :->
	"Process the object event. \nIf the event matches one of the nodes recognisers, use it, otherwise propagate the event to the super class."::
	get(Node, editable, Editable),	
	( Editable = @on
	->	
		( send(@editable_recogniser, event, Ev)
		->	true
		;	get(Ev, key, Key),
			( Key = 'DEL'
			->	send(Node?parent?parent, remove_property, Node?parent)
			;	send(Node, send_super, event, Ev)
			)   
		)
	;	
		( send(@non_editable_recogniser, event, Ev)
		->  true
		;   send(Node, send_super, event, Ev)
		)
	).

:- pce_group(public).

single_clicked(Node) :->
	"Sets the node as selected. \n Fired when the user clicks the node."::
	get(Node, parent, Parent),
	send(Parent?new_nodes, for_all, message(@arg1, colour, black)),
	send(Parent, slot, selected_node, Node),	
	send(Node, colour, red).

double_clicked(Node) :->
	"Sets the node as selected and make connection to itself. \nFired when the user double-clicks the node."::
	send(Node, connect, Node, @self_link, @default, @default),
	get(Node, parent, Parent),
	send(Parent?new_nodes, for_all, message(@arg1, colour, black)),
	send(Parent, slot, selected_node, Node),	
	send(Node, colour, red).

remove_relations(Node) :->
	"Removes all nodes relations."::
	get(Node, connections, Connections),
	send(Connections, for_all, message(@arg1, unlink)).

remove_relation(Node) :->
	"Removes relation between current node and the selected one, if such exists."::
	get(Node, parent, Parent),
	get(Parent, selected_node, SNode),
	SNode \= @nil,
	send(SNode, disconnect, Node, @default, @default, @default).

:- pce_end_class.
