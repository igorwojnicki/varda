:- dynamic ard_att/1.

ard_att('Thermostat').
ard_att('Time').
ard_att('Temperature').
ard_att('Date').
ard_att('Hour').
ard_att(season).
ard_att(operation).
ard_att(thermostat_settings).

:- dynamic ard_property/1.

ard_property(['Thermostat']).
ard_property(['Time', 'Temperature']).
ard_property(['Time']).
ard_property(['Temperature']).
ard_property(['Date', 'Hour', season, operation]).
ard_property(['Date', 'Hour']).
ard_property([season, operation]).

:- dynamic ard_depend/2.

ard_depend(['Date', 'Hour'], [season, operation]).
ard_depend([season, operation], ['Temperature']).
ard_depend(['Temperature'], ['Temperature']).

:- dynamic ard_hist/2.

ard_hist(['Thermostat'], ['Time', 'Temperature']).
ard_hist(['Time', 'Temperature'], ['Time']).
ard_hist(['Time', 'Temperature'], ['Temperature']).
ard_hist(['Time'], ['Date', 'Hour', season, operation]).
ard_hist(['Date', 'Hour', season, operation], ['Date', 'Hour']).
ard_hist(['Date', 'Hour', season, operation], [season, operation]).

