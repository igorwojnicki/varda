%
% $Id: reuse-model.pl,v 1.6 2008-03-06 12:34:36 wojnicki Exp $
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%

varda_model_control :-
    ard_att_add('Control System'),
    ard_property_add(['Control System']),

    ard_att_add('Control1'),
    ard_att_add('Control2'),
    ard_att_add('Calculation'),

    ard_finalize(['Control System'],['Control1','Control2','Calculation']),

    ard_split(['Control1','Control2','Calculation'],
	      [['Control1'],['Control2'],['Calculation']],
	      [
	       [['Control1'],['Calculation']],
	       [['Control2'],['Calculation']],
	       [['Calculation'],['Control1']],
	       [['Calculation'],['Control2']]
	       ]),


    ard_att_add(in1),
    ard_att_add(con1),
    ard_finalize(['Control1'],[in1,con1]),

    ard_att_add(in2),
    ard_att_add(con2),
    ard_finalize(['Control2'],[in2,con2]),

    ard_split([in1,con1],
	      [[in1],[con1]],
	      [
	       [[in1],[con1]],
	       [[in1],['Calculation']],
	       [['Calculation'],[con1]]
	       ]),

    ard_split([in2,con2],
	      [[in2],[con2]],
	      [
	       [[in2],[con2]],
	       [[in2],['Calculation']],
	       [['Calculation'],[con2]]
	       ]),

    ard_att_add(x),
    ard_att_add(y),
    ard_att_add(calc),
    
    ard_finalize(['Calculation'],[x,y,calc]),

    ard_split([x,y,calc],
	      [[x],[y],[calc]],
	      [
	       [[x],[y]],
	       [[y],[calc]],
	       [[in1],[x]],
	       [[in2],[x]],
	       [[calc],[con1]],
	       [[calc],[con2]]
	       ]).
   



:-
	varda_model_control.
%	sar,
%	shi,
%	ax, xa.
%	sxt.
	

