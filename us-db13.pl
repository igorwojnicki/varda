
:- dynamic ard_att/1.

ard_att('NewPolicy').
ard_att('Policy').
ard_att('Client').
ard_att('Premium').
ard_att('PolicyScore').
ard_att('EligibilityScore').
ard_att('AutoEligibility').
ard_att('DriverEligibility').

:- dynamic ard_property/1.

ard_property(['NewPolicy']).
ard_property(['PolicyScore']).
ard_property(['EligibilityScore', 'AutoEligibility', 'DriverEligibility']).
ard_property(['Client', 'PolicyScore', 'Policy', 'Premium']).
ard_property(['Client', 'Policy', 'Premium']).

:- dynamic ard_depend/2.

ard_depend(['EligibilityScore', 'AutoEligibility', 'DriverEligibility'], ['Client', 'Policy', 'Premium']).

:- dynamic ard_hist/2.

ard_hist(['PolicyScore'], ['EligibilityScore', 'AutoEligibility', 'DriverEligibility']).
ard_hist(['Client', 'PolicyScore', 'Policy', 'Premium'], ['PolicyScore']).
ard_hist(['NewPolicy'], ['Client', 'PolicyScore', 'Policy', 'Premium']).
ard_hist(['Client', 'PolicyScore', 'Policy', 'Premium'], ['Client', 'Policy', 'Premium']).
