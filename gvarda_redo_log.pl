%
% $Id: gvarda_redo_log.pl 99 2009-09-27 12:28:34Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_redo_log.pl 99 2009-09-27 12:28:34Z jar0s $'),nl.

:- pce_begin_class(redo_log, object).

variable(operation, name, get, "Name of the operation").
variable(arguments, code_vector, get, "Operation arguments").

initialise(Rl, Operation:name, Arguments:code_vector) :->
	"Creates redo_log object"::
	send(Rl, send_super, initialise),
	send(Rl, slot, operation, Operation),
	send(Rl, slot, arguments, Arguments).

:- pce_end_class.
