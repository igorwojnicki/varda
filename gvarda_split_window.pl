%
% $Id: gvarda_split_window.pl 99 2009-09-27 12:28:34Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_split_window.pl 99 2009-09-27 12:28:34Z jar0s $'),nl.

:- ensure_loaded('gvarda_utils').
:- ensure_loaded('gvarda_split_preview').

:- pce_begin_class(split_window, dialog).

variable(lb, list_browser, get, "List of available attributes.").

initialise(D, Ard:ard_graph, Property:code_vector) :->
	'Creates new split window. Fill list of available attributes and prepares preview area with related nodes from ARD graph.'::	
	create_list_from_code_vector(Property, PList),
	ard_property(PList),
	concat_atom(PList, ', ', Property_name),
	concat_atom(['Split "', Property_name, '"'], '', Title),
	send(D, send_super, initialise, Title),

	new(Lb, list_browser),
	send(Lb, multiple_selection, @on),
	send(Lb, label, 'Available attributes:'),
	send(Property, for_all, message(Lb, append, @arg1)),

	new(Sp, split_preview(D, Property, 400, 300)),

	send(Lb, open_message, message(D, create_property, Lb, Sp)),

	send(D, append, new(LBB, dialog_group(buttons, group))),
	send(LBB, gap, size(0, 10)),
	send(LBB, append, Lb),
	send(LBB, append, button('Create property',
		message(D, create_property, Lb, Sp)), below),
	send(LBB, append, button('Remove property',
		message(D, remove_property, Sp)), below),
	send(LBB, append, button('Clear relations',
		message(Sp, clear_relations)), below),
	send(LBB, layout_dialog),

	send(D, append, new(SPG, dialog_group(preview, group)), right),
	send(SPG, append, Sp),
	send(D, append, button('Done',
		message(D, do_split, Ard, Sp)), next_row),
	send(D, append, button('Cancel',
		message(D?frame, uncreate)), right),
	send(D, slot, lb, Lb),
	send(D, open).

do_split(D, Ard:ard_graph, Sp:split_preview) :->
	"Validates the new properties and performs ard_split() if successful."::
	get(Sp, property, Property),
	create_list_from_code_vector(Property, PList),
	new(Nodes, chain),
	get(Sp, new_nodes, NewNodes),
	( send(NewNodes, for_all, message(@prolog, property_exists, @arg1))
	->	send(NewNodes, for_all, message(Nodes, append, @arg1)),
		chain2l(Nodes, NodesList),
		send(NewNodes, for_all, message(Nodes, append, @arg1)),
		extract_relations(Nodes, [], Relations),	
		( ard_split(PList, NodesList, Relations)
		->	send(Ard, generate),
			send(Ard?parent, log_split, Property, NodesList, Relations),
			send(D?frame, uncreate)
		;	send(@display, report, error, '%s',
					     'Error occured while performing split operation. Please check all relations and try again.')
		)
	;	send(@display, report, error, '%s',
			'Error occured while parsing list of properties. One or more of them already exist.')
	).

create_property(_, Lb:list_browser, Sp:split_preview) :->
	"Creates new property on the preview area from selected attributes."::		
	get(Lb, selection, Chain),
	( \+ send(Chain, empty)
	->	lb2l(Lb, PList),
		concat_atom(PList, '\n', PTxt),
		send(Sp, add_new_node, PTxt, PList)
	;	true
	).

remove_property(D, Sp:split_preview) :->
	'Removes property selected on split preview.'::
	get(D, lb, Lb),	
	get(Sp, selected_node, Node),
	( Node \= @nil
	-> 	get(Node, property, Property),
		send(Sp, erase, Node),
		send(Property, for_all, message(Lb, append, @arg1)),
		send(Node, destroy),
		send(Sp, order_nodes)
	; true
	).
	
:- pce_end_class.
