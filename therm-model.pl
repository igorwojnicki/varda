%
% $Id: therm-model.pl,v 1.7 2008-06-09 09:22:20 gjn Exp $
%
% The well known Thermostat case model from Negnevtisky,
% original analysis  G. J. Nalepa and A. Ligeza
% ARD+ analysis + VARDA modelling by I. Wojnicki and G.J. Nalepa
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


varda_model_desc :- write('The well known Thermostat case model from Negnevtisky, $Id: therm-model.pl,v 1.7 2008-06-09 09:22:20 gjn Exp $').

varda_model_therm :-
	ard_att_add('Thermostat'),
 	ard_property_add(['Thermostat']),

 	ard_att_add('Time'),
	ard_att_add('Temperature'),
 	ard_finalize(['Thermostat'],['Time','Temperature']),

 	ard_split(['Time','Temperature'],[['Time'],['Temperature']],[[['Time'],['Temperature']]]),

	ard_att_add('Date'),
	ard_att_add('Hour'),
	ard_att_add(season),
	ard_att_add(operation),
	ard_finalize(['Time'],['Date','Hour',season,operation]),

 	ard_split(['Date','Hour',season,operation],
		  [['Date','Hour'],[season,operation]],
		  [[['Date','Hour'],[season,operation]],[[season,operation],['Temperature']]]),

	ard_split([season,operation],
		  [[season],[operation]],
		  [[['Date','Hour'],[season]],
		   [['Date','Hour'],[operation]],
		   [[season],['Temperature']],
		   [[operation],['Temperature']]]),

	ard_split(['Date','Hour'],
		  [['Date'],['Hour']],
		  [[['Date'],[season]],
		   [['Date'],[operation]],
		   [['Hour'],[operation]]]),

  	ard_att_add(day),
  	ard_att_add(month),
  	ard_att_add(today),
	
  	ard_finalize(['Date'],[day,month,today]),

	ard_split([day,month,today],
		  [[month],[day,today]],
		  [[[month],[season]],
		   [[day,today],[operation]]]),

	ard_split([day,today],
		  [[day],[today]],
		  [[[day],[today]],
		   [[today],[operation]]]),

	ard_att_add(hour),
	ard_finalize(['Hour'],[hour]),

  	ard_att_add(thermostat_settings),
  	ard_finalize(['Temperature'],[thermostat_settings]).

:- varda_model_therm.
