%
% $Id: userv-model-alpha.pl,v 1.10 2008-03-06 12:34:36 wojnicki Exp $
%
% The famous UServ case from BRForum, see
% http://www.businessrulesforum.com/2005_Product_Derby.pdf
% original analysis by G. J. Nalepa
% ARD+ analysis + VARDA modelling by G.J. Nalepa,
% with help from I. Wojnicki
% related design by A. Giurca and the team from REWERSE I1
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%

varda_model_userv :-
	userv_flairs,
	userv_refactor1,
	userv_more1,
	userv_refactor2,
%%	userv_refactor3, OLD
	userv_refactor3new,	
	userv_more2_ugly,
	userv_finalize_remains.

varda_model_desc :- write('The famous UServ case from BRForum, $Id: userv-model-alpha.pl,v 1.10 2008-03-06 12:34:36 wojnicki Exp $').

userv_flairs :-
	
	% Level  0
	ard_att_add('NewPolicy'),
	ard_property_add(['NewPolicy']),

	% Level  1
	ard_att_add('Policy'),
	ard_att_add('Premium'),
	ard_att_add('PolicyScore'),
 	ard_finalize(['NewPolicy'],
		     ['PolicyScore','Policy','Premium']),

	% Level  2
	ard_split(['PolicyScore','Policy','Premium'],
		  [['PolicyScore'],['Policy','Premium']],
		  [[['PolicyScore'],['Policy','Premium']]]),

	% Level  3
	ard_att_add('EligibilityScore'),
	ard_att_add('AutoEligibility'),
	ard_att_add('DriverEligibility'),
	ard_finalize(['PolicyScore'],
		     ['EligibilityScore','AutoEligibility','DriverEligibility']),

	% Level  4
	ard_split(['EligibilityScore','AutoEligibility','DriverEligibility'],
		  [['EligibilityScore'],['AutoEligibility','DriverEligibility']],
		  [
		   [['AutoEligibility','DriverEligibility'],['EligibilityScore']],
		   [['EligibilityScore'],['Policy','Premium']]
		  ]),

	% Level  5
	ard_split(['AutoEligibility','DriverEligibility'],
		  [['AutoEligibility'],['DriverEligibility']],
		  [
		   [['AutoEligibility'],  ['EligibilityScore']],
		   [['DriverEligibility'],['EligibilityScore']]
		   ]),

	
	% Level  6
	ard_att_add('AutoEligibilityScore'),
	ard_att_add('Car'),
	ard_finalize(['AutoEligibility'],
		     ['AutoEligibilityScore','Car']),
	
	ard_att_add('DriverEligibilityScore'),
	ard_att_add('Driver'),
	ard_finalize(['DriverEligibility'],
		     ['DriverEligibilityScore','Driver']),

	% Level  7
	ard_split(['AutoEligibilityScore','Car'],
		  [['AutoEligibilityScore'],['Car']],
		  [
		   [['Car'],['AutoEligibilityScore']],
		   [['AutoEligibilityScore'],['EligibilityScore']]
		  ]),
	ard_split(['DriverEligibilityScore','Driver'],
		  [['DriverEligibilityScore'],['Driver']],
		  [
		   [['Driver'],['DriverEligibilityScore']],
		   [['DriverEligibilityScore'],['EligibilityScore']]
		  ]),

	% THE SEMANTIC GAP!

	% Level  8
	ard_att_add('PotentialTheftCategory'),
	ard_att_add('PotentialOccupantInjuryCategory'),
	ard_finalize(['Car'],
		     ['PotentialTheftCategory','PotentialOccupantInjuryCategory']),
	
	ard_att_add('DriverAgeCategory'),
	ard_att_add('DrivingRecordCategory'),
	ard_finalize(['Driver'],
		     ['DriverAgeCategory','DrivingRecordCategory']),

	% Level  9
	ard_split(['PotentialTheftCategory','PotentialOccupantInjuryCategory'],
		  [['PotentialTheftCategory'],['PotentialOccupantInjuryCategory']],
		  [
		   [['PotentialTheftCategory'],['AutoEligibilityScore']],
		   [['PotentialOccupantInjuryCategory'],['AutoEligibilityScore']]
		  ]),
	ard_split(['DriverAgeCategory','DrivingRecordCategory'],
		  [['DriverAgeCategory'],['DrivingRecordCategory']],
		  [
		   [['DriverAgeCategory'],['DriverEligibilityScore']],
		   [['DrivingRecordCategory'],['DriverEligibilityScore']]
		  ]),

	% Level 10
	ard_att_add('carConvertible'),
	ard_att_add('carPrice'),
	ard_att_add('carType'),
	ard_att_add('carOnList'),
	ard_att_add('potentialTheftRating'),
	ard_finalize(['PotentialTheftCategory'],
		     ['potentialTheftRating','carConvertible','carPrice','carType','carOnList']),

	ard_att_add('carAirbags'),
	ard_att_add('carRollBar'),
	ard_att_add('potentialOccupantInjuryRating'),
	ard_finalize(['PotentialOccupantInjuryCategory'],
		     ['potentialOccupantInjuryRating','carAirbags','carRollBar']),
	
	ard_att_add('driverSex'),
	ard_att_add('driverAge'),
	ard_att_add('trainingCertificate'),
	ard_att_add('driverAgeRating'),
	ard_finalize(['DriverAgeCategory'],
		     ['driverAgeRating','driverSex','driverAge','trainingCertificate']),
	
	ard_att_add('accidentsNumber'),
	ard_att_add('violationsNumber'),
	ard_att_add('convictedDUI'),
	ard_att_add('driverQualification'),
	ard_finalize(['DrivingRecordCategory'],
		     ['driverQualification','accidentsNumber','violationsNumber','convictedDUI']),

	ard_att_add('theAutoEligibilityScore'),
	ard_finalize(['AutoEligibilityScore'],
		     ['theAutoEligibilityScore']),
	
	ard_att_add('theDriverEligibilityScore'),
	ard_finalize(['DriverEligibilityScore'],
		     ['theDriverEligibilityScore']),
	
	ard_split(['Policy','Premium'],
		  [['Policy'],['Premium']],
		  [
%		   [['Policy'],['Premium']],
		   [['Premium'],['Policy']],		   
%		   [['EligibilityScore'],['Policy']] was in the rig flairs submission
		   [['EligibilityScore'],['Premium']],
		   [['EligibilityScore'],['Policy']]		  		  
		  ]),
	
	% Level 11
	ard_split(['potentialTheftRating','carConvertible','carPrice','carType','carOnList'],
		  [['potentialTheftRating'],['carConvertible','carPrice','carType','carOnList']],
		  [
		   [['carConvertible','carPrice','carType','carOnList'],['potentialTheftRating']],
		   [['potentialTheftRating'],['theAutoEligibilityScore']]
		  ]),
	ard_split(['potentialOccupantInjuryRating','carAirbags','carRollBar'],		  
		  [['potentialOccupantInjuryRating'],['carAirbags','carRollBar']],		  
		  [
		   [['carAirbags','carRollBar'],['potentialOccupantInjuryRating']],		   
		   [['potentialOccupantInjuryRating'],['theAutoEligibilityScore']]
		  ]),
	ard_split(['driverAgeRating','driverSex','driverAge','trainingCertificate'],
		  [['driverAgeRating'],['driverSex','driverAge','trainingCertificate']],
		  [
		   [['driverSex','driverAge','trainingCertificate'],['driverAgeRating']],
		   [['driverAgeRating'],['theDriverEligibilityScore']]
		  ]),	
	ard_split(['driverQualification','accidentsNumber','violationsNumber','convictedDUI'],
		  [['driverQualification'],['accidentsNumber','violationsNumber','convictedDUI']],
		  [
		   [['accidentsNumber','violationsNumber','convictedDUI'],['driverQualification']],
		   [['driverQualification'],['theDriverEligibilityScore']]
		  ]),

	ard_att_add('thePremium'),	
	ard_att_add('AutoPremiums'),
	ard_att_add('AutoDiscounts'),
	ard_att_add('DriverPremiums'),
	ard_att_add('MarketSegmentDiscounts'),
	ard_finalize(['Premium'],
		     ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']).

% described in the FLAIRS2008 paper submission
userv_refactor1 :-
	ard_depend_add(['potentialOccupantInjuryRating'],
		       ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']),
	ard_depend_add(['driverAgeRating'],
		       ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']).

userv_more1 :-
	ard_split(['carConvertible','carPrice','carType','carOnList'],
		  [['carConvertible'],['carPrice'],['carType'],['carOnList']],
		  [
		   [['carConvertible'],['potentialTheftRating']],
		   [['carPrice'],['potentialTheftRating']],
		   [['carType'],['potentialTheftRating']],
		   [['carOnList'],['potentialTheftRating']]
		  ]),
	ard_split(['carAirbags','carRollBar'],
		  [['carAirbags'],['carRollBar']],
		  [
		   [['carAirbags'],['potentialOccupantInjuryRating']],
		   [['carRollBar'],['potentialOccupantInjuryRating']]
		  ]),
	ard_split(['driverSex','driverAge','trainingCertificate'],
		  [['driverSex'],['driverAge'],['trainingCertificate']],
		  [
		   [['driverSex'],['driverAgeRating']],
		   [['driverAge'],['driverAgeRating']],
		   [['trainingCertificate'],['driverAgeRating']]
		  ]),	
	ard_split(['accidentsNumber','violationsNumber','convictedDUI'],
		  [['accidentsNumber'],['violationsNumber'],['convictedDUI']],
		  [
		   [['accidentsNumber'],['driverQualification']],
		   [['violationsNumber'],['driverQualification']],
		   [['convictedDUI'],['driverQualification']]
		  ]),
	
	ard_att_add('theEligibilityScore'), %fixme, withut this one the next breaks, instead of failing?
	ard_finalize(['EligibilityScore'],
		     ['theEligibilityScore']).

userv_refactor2 :-
	ard_depend_add(['potentialTheftRating'],
				     ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']),
	ard_depend_add(['carAirbags'],
				     ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']),
	ard_depend_add(['driverQualification'],
				     ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']),
	ard_depend_add(['accidentsNumber'],
				     ['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts']).

userv_refactor3_ugly1 :-
%	refactor_finalize_property_insert()
	refactor_finalize_trivial('carAge',
				  ['Car'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	refactor_finalize_trivial('carModelYear',
				  ['Car'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	refactor_finalize_trivial('carHasAlarm',
				  ['Car'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),

	refactor_finalize_trivial('driverMaritalStatus',
				  ['Driver'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	refactor_finalize_trivial('driverLocation',
				  ['Driver'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	refactor_finalize_trivial('driverCoverage',
				  ['Driver'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),

	refactor_finalize_trivial('clientClass',
				  ['Car'], ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).
%	refactor_finalize_trivial('clientRevenue',
%				  'Car', ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
%	refactor_finalize_trivial('clientPortfolio',
%				  'Car', ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
userv_refactor3new :-
	userv_refactor31new, 
	userv_refactor32new, 
	userv_refactor33new. 
	
userv_refactor31new :-
	ard_att_add('CarPremiums'),
	ard_refactor_attribute_add('CarPremiums',[['PotentialTheftCategory','PotentialOccupantInjuryCategory']]),
	ard_att_add_list(['carAge','carModelYear','carHasAlarm']),
	ard_refactor_finalize_add(['CarPremiums'],['carAge','carModelYear','carHasAlarm']),
	
	ard_refactor_split_add(['carAge','carModelYear','carHasAlarm'],[['carAge'],['carModelYear'],['carHasAlarm']]),
	
	ard_depend_add(['carAge'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['carModelYear'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['carHasAlarm'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).

userv_refactor32new :-

	ard_att_add_list(['driverMaritalStatus','driverLocation','driverCoverage']),
	ard_refactor_attribute_add('driverMaritalStatus',
			  [['DriverAgeCategory','DrivingRecordCategory']]),
	ard_refactor_attribute_add('driverLocation',
			  [['driverMaritalStatus','DriverAgeCategory','DrivingRecordCategory']]),
	ard_refactor_attribute_add('driverCoverage',
			  [['driverLocation','driverMaritalStatus','DriverAgeCategory','DrivingRecordCategory']]),
	
	ard_depend_add(['driverMaritalStatus'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['driverLocation'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['driverCoverage'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).

userv_refactor33new :-
	ard_att_add('Client'),
	ard_refactor_attribute_add('Client',[['PolicyScore','Policy','Premium'],['Policy','Premium']]),

	ard_att_add_list(['clientClass','clientRevenue','clientPortfolio']),
	ard_refactor_finalize_add(['Client'],['clientClass','clientRevenue','clientPortfolio']),
	
	ard_refactor_split_add(['clientClass','clientRevenue','clientPortfolio'],[['clientClass'],['clientRevenue'],['clientPortfolio']]),

	ard_depend_add(['clientRevenue'],['clientClass']),
	ard_depend_add(['clientPortfolio'],['clientClass']),
	ard_depend_add(['clientClass'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
userv_refactor3 :-
	userv_refactor31, 
	userv_refactor32, 
	userv_refactor33. 
	
userv_refactor31 :-
	ard_att_add('CarPremiums'),
	ard_hist_append('CarPremiums',['PotentialTheftCategory','PotentialOccupantInjuryCategory']),
	refactor_split(['CarPremiums'],['CarPremiums','PotentialTheftCategory','PotentialOccupantInjuryCategory']),
	
	ard_att_add('carAge'),
	ard_att_add('carModelYear'),
	ard_att_add('carHasAlarm'),
	refactor_finalize_add(['CarPremiums'],['carAge','carModelYear','carHasAlarm']),
	
	refactor_split_final(['carAge','carModelYear','carHasAlarm'],['carAge','carModelYear','carHasAlarm']),

	ard_depend_add(['carAge'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['carModelYear'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['carHasAlarm'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).

userv_refactor32 :-

%	ard_att_add('DriverPremiums'),
%	refactor_split(['DriverPremiums'],['DriverX']),

	ard_att_add('driverMaritalStatus'),
	ard_att_add('driverLocation'),
	ard_att_add('driverCoverage'),
	refactor_finalize('driverMaritalStatus',
			  [['DriverAgeCategory','DrivingRecordCategory']]),
	refactor_finalize('driverLocation',
			  [['driverMaritalStatus','DriverAgeCategory','DrivingRecordCategory']]),
	refactor_finalize('driverCoverage',
			  [['driverLocation','driverMaritalStatus','DriverAgeCategory','DrivingRecordCategory']]),
	refactor_split_final(['driverMaritalStatus','driverLocation','driverCoverage'],['driverCoverage','driverLocation','driverMaritalStatus','DriverAgeCategory','DrivingRecordCategory']),

	ard_depend_add(['driverMaritalStatus'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['driverLocation'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']),
	ard_depend_add(['driverCoverage'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).

userv_refactor33 :-

	ard_att_add('Client'),
%	refactor_finalize('Client',[['PolicyScore','Policy','Premium'],['Policy','Premium'],['Premium']]),
	refactor_finalize('Client',[['PolicyScore','Policy','Premium'],['Policy','Premium']]),	
	refactor_split(['Client'],['Client','Policy','Premium']),


	ard_att_add('clientClass'),
	ard_att_add('clientRevenue'),
	ard_att_add('clientPortfolio'),
	refactor_split_final(['clientClass','clientRevenue','clientPortfolio'],['Client']),

	ard_depend_add(['clientRevenue'],['clientClass']),
	ard_depend_add(['clientPortfolio'],['clientClass']),
	ard_depend_add(['clientClass'],
		       ['thePremium', 'AutoPremiums', 'AutoDiscounts', 'DriverPremiums', 'MarketSegmentDiscounts']).
	

userv_more2_ugly :-
	ard_split(['thePremium','AutoPremiums','AutoDiscounts','DriverPremiums','MarketSegmentDiscounts'],
		  [['thePremium'],['AutoPremiums'],['AutoDiscounts'],['DriverPremiums'],['MarketSegmentDiscounts']],
		  [
		   [['thePremium'],['Policy']],
		   
		   [['carType'],['AutoPremiums']],
		   [['carModelYear'],['AutoPremiums']],		   		   
		   [['carAge'],['AutoPremiums']],
		   [['driverCoverage'],['AutoPremiums']],
		   [['potentialTheftRating'],['AutoPremiums']],
		   [['potentialOccupantInjuryRating'],['AutoPremiums']],

		   [['AutoPremiums'],['AutoDiscounts']],
		   [['carAirbags'],['AutoDiscounts']],
		   [['potentialTheftRating'],['AutoDiscounts']],
		   [['carHasAlarm'],['AutoDiscounts']],
		   [['AutoDiscounts'],['thePremium']],
		   
		   [['driverQualification'],['DriverPremiums']],
		   [['driverAgeRating'],['DriverPremiums']],
		   [['driverMaritalStatus'],['DriverPremiums']],
		   [['driverLocation'],['DriverPremiums']],
		   [['accidentsNumber'],['DriverPremiums']],
		   [['DriverPremiums'], ['thePremium']],
		   
		   [['clientClass'],['MarketSegmentDiscounts']],
		   [['MarketSegmentDiscounts'],['thePremium']]
		   
		   ]).

userv_finalize_remains :-
	ard_att_add('theAutoPremiums'),
	ard_transform_finalize(['AutoPremiums'],['theAutoPremiums']),
	ard_att_add('theDriverPremiums'),
	ard_transform_finalize(['DriverPremiums'],['theDriverPremiums']),
	ard_att_add('theAutoDiscounts'),
	ard_transform_finalize(['AutoDiscounts'],['theAutoDiscounts']),
	ard_att_add('theMarketSegmentDiscounts'),
	ard_transform_finalize(['MarketSegmentDiscounts'],['theMarketSegmentDiscounts']),
	ard_att_add('thePolicy'),
	ard_transform_finalize(['Policy'],['thePolicy']).

:- varda_model_userv.
% missed:
%  After all discounts have been applied, the annual premium must be greater than or equal to the sum of the base premium for all the cars on the policy. see page 7, top
%  If the client is eligible for auto insurance, then the annual premium must be determined., page 5

% before refactor 2
% here all of the other problems begin

% 	AutoPremiums
% carClass
% carYear
% carAge
% carInsurance
% potentialOccupantInjuryRating
% potentialTheftRating %ref

% AutoDisounts
% carAirbags %ref
% potentialTheftRating %ref
% carAlarmSystem

% DriverPremiums
% driverAgeRating %ref
% driverLocation
% driverMartial
% driverQualification %ref

% MarketSegmentDiscounts
% ClientQulification

% ClientQulification
% clientRevenue
% clientPortfolio

% run userv-full-alpha + userv_refactor2
% we blew it on the 9th level

