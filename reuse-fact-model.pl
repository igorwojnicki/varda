%
% $Id: reuse-fact-model.pl,v 1.3 2008-03-02 23:14:55 gjn Exp $
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:-ensure_loaded(varda).

varda_model_control :-
    ard_att_add('Control System'),
    ard_property_add(['Control System']),

    ard_att_add('Control1'),
    ard_att_add('Control2'),
    ard_att_add('Factorial'),

    ard_finalize(['Control System'],['Control1','Control2','Factorial']),

    ard_split(['Control1','Control2','Factorial'],
	      [['Control1'],['Control2'],['Factorial']],
	      [
	       [['Control1'],['Factorial']],
	       [['Control2'],['Factorial']],
	       [['Factorial'],['Control1']],
	       [['Factorial'],['Control2']]
	       ]),


    ard_att_add(in1),
    ard_att_add(con1),
    ard_finalize(['Control1'],[in1,con1]),

    ard_att_add(in2),
    ard_att_add(con2),
    ard_finalize(['Control2'],[in2,con2]),

    ard_split([in1,con1],
	      [[in1],[con1]],
	      [
	       [[in1],[con1]],
	       [[in1],['Factorial']],
	       [['Factorial'],[con1]]
	       ]),

    ard_split([in2,con2],
	      [[in2],[con2]],
	      [
	       [[in2],[con2]],
	       [[in2],['Factorial']],
	       [['Factorial'],[con2]]
	       ]),

    ard_att_add(x),
    ard_att_add(s),
    ard_att_add(y),
    
    ard_finalize(['Factorial'],[x,s,y]),

    ard_split([x,s,y],
	      [[x],[s],[y]],
	      [
	       [[x],[y]],
	       [[x],[s]],
	       [[s],[s]],
	       [[y],[y]],
	       [[s],[y]],
	       [[in1],[x]],
	       [[in2],[x]],
	       [[y],[con1]],
	       [[y],[con2]]
	       ]).
   



:-
	varda_model_control.
%	sar,
%	shi,
%	ax, xa.
%	sxt.
	

