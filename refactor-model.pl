%
% $Id: refactor-model.pl,v 1.3 2008-03-02 23:14:54 gjn Exp $
%
% A toy model for refactoring by G. J. Nalepa
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- ensure_loaded('varda').

% by the way, this model is carzy, when it comes to hic :( ?

varda_model_refactor :-
	creat_ref1,
	ref1,
	ref2,
	ref3.

varda_model_desc :- write('A toy model for refactoring by G. J. Nalepa, $Id: refactor-model.pl,v 1.3 2008-03-02 23:14:54 gjn Exp $').


creat_ref1 :-
	ard_att_add('A'),
	ard_property_add(['A']),

	ard_att_add_list(['B','C','D','E']),
	ard_finalize(['A'],['B','C','D','E']),
	
	ard_split(['B','C','D','E'],
		  [['B','C'],['D'],['E']],
		  [
		   [['B','C'],['D']],
		   [['B','C'],['E']]
		   ]),

	ard_att_add('e'),
	ard_finalize(['E'],['e']),
	% a double node can only be split to two! maybe this could be done automagically?
	ard_split(['B','C'],
		  [['B'],['C']],
		  [
		   [['B'],['D']],
		   [['B'],['e']],
		   [['C'],['e']]
		  ]),


	ard_att_add_list(['F','g','b']),
	ard_finalize(['D'],['F','g']),
	ard_finalize(['B'],['b']),

	true.

ref1 :-
	ard_att_add('Alpha'),
	ard_refactor_attribute_add('Alpha',[['D']]),
	
	ard_att_add('beta'),
	
% these two are wrong! they corrupt the TPH!
% but the current code allows this :(
% see how to fix ard_hist_append_to_nodes
%	ard_refactor_attribute_add('beta',[['B','C'],['B'],['b']]),
%	ard_refactor_attribute_add('beta',[['B','C'],['B']]),	

% this one is good but simple
	ard_refactor_attribute_add('beta',[['B','C']]),	
	ard_depend_add(['e'],['beta']).

ref2 :-
	ard_att_add_list(['H','i']),
	ard_refactor_finalize_add(['Alpha'],['H','i']).

ref3 :-
	ard_refactor_split_add(['H','i'],[['H'],['i']]),

	ard_att_add('h'),
	ard_refactor_finalize_add(['H'],['h']),

	ard_depend_add(['F','g'],['h']),
	ard_depend_add(['F','g'],['i']).

:- varda_model_refactor.
