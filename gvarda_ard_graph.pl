%
% $Id: gvarda_ard_graph.pl 112 2009-09-28 15:14:59Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_ard_graph.pl 112 2009-09-28 15:14:59Z jar0s $'),nl.

:- use_module(library(hyper)).

:- ensure_loaded('gvarda_finalize_window').
:- ensure_loaded('gvarda_split_window').

:- pce_begin_class(ard_graph, picture).

variable(nodes, chain, get, "List of all nodes of the graph.").
variable(selected_node, graph_node*, get, "Currently selected node.").
variable(x, int, get, "X coordinate of next node will be placed at.").
variable(y, int, get, "Y coordinate of next node will be placed at.").
variable(x_offset, int, get, "Offset value used for nodes layout.").
variable(y_offset, int, get, "Offset value used for nodes layout.").
variable(parent, gvarda_main_window, get, "Parent object").

initialise(ARDgraph, GMW:gvarda_main_window, Width:int, Height:int) :->
	"Create ard-graph"::
	send(ARDgraph, send_super, initialise, 'ARD', size(Width, Height)),
	send(ARDgraph, slot, nodes, new(_, chain)),
	send(ARDgraph, slot, selected_node, @nil),
	send(ARDgraph, slot, parent, GMW),
	send(ARDgraph, recogniser, 
		click_gesture(left, '', single, message(ARDgraph, single_clicked))
	).

:- pce_group(internal).

clear(P) :->
	"Clear the diagram"::
	send(P, send_super, clear).

layout(P) :->
	"Run graph layout"::	
	get_ard_root(Property),
	concat_atom(Property, '\n', Name),
	( get(P, member, Name, Node)
	->	get(P, x, X),
		get(P, y, Y),
		send(Node, request_geometry, X, Y, @default, @default),
		send(P, slot, x, X + Node?width + 50),
		send(P, position_related, Node)
	;	true
	).

position_related(P, Node:graph_node) :->
	"Position nodes related to the Node on graph"::
	get(Node?property, copy, Property),
	create_list_from_code_vector(Property, PList),
	forall(ard_depend(RList, PList),
	    (
		( \+ RList = PList
		->	concat_atom(RList, '\n', Name),
			( get(P, member, Name, RelatedNode)
			->	get(RelatedNode, positioned, Positioned),
				( Positioned == @off
				->	get(P, x, X),
					get(P, y, Y),
					send(RelatedNode, request_geometry, X, Y, @default, @default),
					send(P, slot, x, X + RelatedNode?width + 50),
					send(P, slot, x_offset, RelatedNode?width + 50),
					send(P, slot, y_offset, RelatedNode?height + 10),
					send(RelatedNode, slot, positioned, @on),
					send(P, position_related, RelatedNode)
				;	true
				)
			;	true
			)
		;	true
		)
	    )),
	get(P, x, X),
	get(P, y, Y),
	get(P, x_offset, Xoffset),
	get(P, y_offset, Yoffset),
	send(P, slot, x, X - Xoffset),
	( ard_depend(_, PList)
	-> send(P, slot, y, Y + Yoffset)
	; true
	).

display_property(P, Property:code_vector) :->
	"Display property if it is a leaf of TPH tree."::
	create_list_from_code_vector(Property, PList),
	( ard_hist(PList, _)
	-> true
   	; concat_atom(PList, '\n', PText),
   	  get(P, node, PText, PList, _)
	).

%create gobal links objects. To be used for connecting the nodes.
:- pce_global(@graph_link, new(link(from, to, line(arrows := second)))).
:- pce_global(@node_link, new(link(from, middle, line(arrows := second)))).

display_arc(P, From:name, FromP:code_vector, To:name, ToP:code_vector) :->
	"Connect two nodes. From -> To"::
	get(P, node, From, FromP, NF),
	( From == To
	->	send(NF, handle, handle(0, h/2, from)),
		send(NF, handle, handle(w/2, 0, middle)),
		send(NF, connect, NF, @node_link)
	;	get(P, node, To, ToP, TF),
		send(NF, handle, handle(0, h/2, from)),
		send(TF, handle, handle(w, h/2, to)),
		send(NF, connect, TF, @graph_link)
	).

node(P, Name:name, Property:code_vector, Node:graph_node) :<-
	"Get (create) node with specified name"::
	(   get(P, member, Name, Node)
	->  true
	;   new(Node, graph_node(Name, Property, P)),
	    send(P, display, Node),
	    get(P, nodes, Nodes),
	    send(Nodes, add, Node),
	    send(P, slot, nodes, Nodes)
	).

single_clicked(P) :->
	"Uncheck all nodes. \nFired when the user clicks on the graph outside of the nodes."::
	send(P, slot, selected_node, @nil),
	get(P, nodes, Nodes),
	send(Nodes, for_all, message(@arg1, colour, black)),
	send(P?parent?tph, select_node, "").

:- pce_group(public).

generate(P) :->
	"Repaints the graph."::
	send(P, clear),

	get(P, visible, area(X, Y, _, _)),
	send(P, slot, x, X + 5),
	send(P, slot, y, Y + 5),
	send(P, slot, x_offset, 0),
	send(P, slot, y_offset, 0),

	forall(ard_property(Property),
	   (
		send(P, display_property, Property)
	   )),
	forall(ard_depend(From, To),
	   (
	   concat_atom(From, '\n', FromText),
	   concat_atom(To, '\n', ToText),
	   send(P, display_arc, FromText, From, ToText, To)
	   )),
	send(P, layout),
	send(P, slot, selected_node, @nil),
	send(P?parent?tph, generate).

has_selected(P) :->
	"Test if any node is currently selected."::
	get(P, selected_node, Node),	
	( Node \= @nil
	-> true
	; false
	).
	
:- pce_end_class.


:- pce_begin_class(graph_node(name, chain, ard_graph), figure).

handle(w/2, 0, link, link).

variable(property, code_vector, get, "Property that particular node represents.").
variable(parent, ard_graph, get, "Graph that particular node belogns to").
variable(positioned, bool, get, "Flag indicating whether node was already positioned on the ard_graph or not").

initialise(Node, Name:name, Property:code_vector, Parent:ard_graph) :->
	"Create from name"::
	send(Node, send_super, initialise),
	send(Node, display, text(Name, center)),
	send(Node, border, 5),
	send(Node, pen, 2),
	send(Node, alignment, center),
	send(Node, send_super, name, Name),
	send(Node, slot, property, Property),
	send(Node, slot, parent, Parent),
	send(Node, slot, positioned, @off).

:- pce_group(internal).

name(Node, Name:name) :->
	"Change name of a node"::
	get(Node, member, text, Text),
	send(Text, string, Name),
	send(Node, send_super, name, Name).

open_dialog_window(Node) :->
	"Check whether perform finalize or split operation and open proper dialog. \nDisplays an error message in case no ARD operation could be performed on the node (the ARD property it represents is physical)."::
	get(Node, property, Property),
	(send(Node, can_finalize)
	-> 	get(Node, parent, Parent),
		new(_, finalize_window(Parent, Property))
	; ( send(Node, can_split)
	 	->	get(Node, parent, Parent),
			new(_, split_window(Parent, Property))
		;	% else open modal dialog "No valid operation"
			send(@display, inform, '%s',
				     'There is no valid operation that could be performed on selected node.')		
	  )			
	).

%create object event recogniser and append actions to it.
:- pce_global(@graph_node_recogniser, make_graph_node_recogniser).

make_graph_node_recogniser(R) :-
	new(R, handler_group(
				click_gesture(left, '', double, message(@receiver, double_clicked)),
				click_gesture(left, '', single, message(@receiver, single_clicked)),
				move_gesture(left)
			)).

event(Node, Ev:event) :->
	"Process the object event. \nIf the event matches the nodes recogniser, use it, otherwise propagate the event to the super class."::
	(   send(@graph_node_recogniser, event, Ev)
	->  true
	;   send(Node, send_super, event, Ev)
	).

:- pce_group(public).

can_finalize(Node) :->
	"Test whether node can be finalized."::
	get(Node, property, Property),
	get(Property, low_index, Lindex),
	get(Property, high_index, Hindex),
	(Lindex = Hindex
	-> 	get(Property, head, Element),
		atom_codes(Element, Codes),	
		[Code|_] = Codes,
		(Code < 91
		-> true
		; false			
		)			
	; false
	).

can_split(Node) :->
	"Test whether node can be split."::
	get(Node, property, Property),
	get(Property, low_index, Lindex),
	get(Property, high_index, Hindex),
	(Lindex = Hindex
	-> false			
	; true
	).

double_clicked(Node) :->
	"Sets the node as selected and calls open_dialog_window. \nFired when the user double-clicks on the node."::
	get(Node, parent, Parent),
	get(Parent, nodes, Nodes),	
	send(Nodes, for_all, message(@arg1, colour, black)),
	send(Node, colour, red),
	get(Node, name, Name),
	send(Parent?parent?tph, select_node, Name),
	send(Node, open_dialog_window).

single_clicked(Node) :->
	"Sets the node as selected. Fired when the user clicks on the node."::
	get(Node, parent, Parent),
	get(Parent, nodes, Nodes),	
	send(Nodes, for_all, message(@arg1, colour, black)),
	send(Parent, slot, selected_node, Node),
	send(Node, colour, red),
	get(Node, name, Name),
	send(Parent?parent?tph, select_node, Name).

:- pce_end_class.
