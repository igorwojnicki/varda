%
% $Id: varda_xtt.pl,v 1.17 2008-03-13 23:10:07 wojnicki Exp $
%
% Copyright 2007,8 by Grzegorz J. Nalepa and Igor Wojnicki
%
% VARDA model transformation predicates
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


:- write('VARDA: $Id: varda_xtt.pl,v 1.17 2008-03-13 23:10:07 wojnicki Exp $'),nl.

:- dynamic(xtt/2).

% purge xtt prototype
pux :- retractall(xtt(_,_)).

gax :- pux,ax,xa.
xop :- xtt_opt.

% generate xtt, ard_depend(_,_) are retracted

ax :-
	forall(ard_depend(From,To),
	       (
		From = [Fa], To = [Ta],
		atom(Fa), atom(Ta)
	       )),
	ax_helper.

ax_helper :-
	ard_depend(F,T),
	F \= T,
	ard_xtt(F,T),
	fail.
ax_helper.

%print xtt
pxt:-
	xtt(F,T),
	write(F), write('|'), write(T), nl,
	fail.
pxt.

% optimize xtt
xtt_opt:-
	xtt(F1,T1),
	xtt(F2,T2),
	sort(F1,F),
	sort(F2,F),
	T1 \= T2,
	retract(xtt(F1,T1)),
	retract(xtt(F2,T2)),
	merge(T1,T2,M),
	assert(xtt(F,M)),
	xtt_opt,
	fail.
xtt_opt:-
	xtt(F1,T1),
	xtt(F2,T2),
	sort(T1,T),
	sort(T2,T),
	F1 \= F2,
	retract(xtt(F1,T1)),
	retract(xtt(F2,T2)),
	merge(F1,F2,M),
	assert(xtt(M,T)),
	xtt_opt,
	fail.
xtt_opt.

%generate ard from xtt
xa:-
	xtt(F,T),
	ard_gen(F,T),
	fail.
xa.

% show xtt
sxt:-
	once(xtt(_,_)),
	tell(pipe('dot -Tpng | display -title XTT &')),
	dotgen_xtt,
	told.

sxt(Name):-
	xtt(_,_),
%	concat(Name,'.dot',File),
	tell(Name),
	dotgen_xtt,
	told.

% helper
ft(F,T,FT):- ard_depend(F,T), ard_depend(FT,T), FT \=F.

% helper
tf(F,T,TF):- ard_depend(F,T), ard_depend(F,TF), TF \= T,
	\+ ( ard_depend(X,TF), X \= F).


% geenrate xtt from a dependecy: F,T
ard_xtt(F,T):- 
	ard_depend(F,T),
	\+ tf(F,T,_),
	\+ ft(F,T,_),
	assert(xtt([F],[T])),
	ard_done([F],[T]).
ard_xtt(F,T):-
	ard_depend(F,T),
	\+ tf(F,T,_),
	findall(FT,ft(F,T,FT),ListFT),
	assert(xtt([F|ListFT],[T])),
	ard_done([F|ListFT],[T]).
ard_xtt(F,T):-
	ard_depend(F,T),
	\+ft(F,T,_),
	findall(TF,tf(F,T,TF),ListTF),
	assert(xtt([F],[T|ListTF])),
	ard_done([F],[T|ListTF]).
ard_xtt(F,T):-
	ard_depend(F,T),
	findall(TF,tf(F,T,TF),ListTF),
	findall(FT,ft(F,T,FT),ListFT),
	assert(xtt([F|ListFT],[T])),
	assert(xtt([F],ListTF)),
	ard_done([F|ListFT],[T]),
	ard_done([F],ListTF).
	
ard_done(F,T):-
	member(FM,F),
	member(TM,T),
	retract(ard_depend(FM,TM)),
	fail.
ard_done(_,_).



ard_gen(F,T):-
	member(FM,F),
	member(TM,T),
	assert(ard_depend(FM,TM)),
	fail.
ard_gen(_,_).


dotgen_xtt:-
	dotgen_xtt_head,
	dotgen_xtt_body,
	dotgen_xtt_foot.

dotgen_xtt_head:-
	write('digraph G{  node [shape=record, fontsize=9, height=0.1, width=0.05]; edge [style=dashed, arrowsize=0.5, len=0.1]; rankdir=LR '), nl.

dotgen_xtt_foot:-
	write('}'), nl.

% xtts
dotgen_xtt_body:-
	xtt(F,T),
	xtt_belongs_to_group(F,T,G),
	write('subgraph "cluster_'),write(G),write('" {'),nl,
	write('"{'), write(F), write('|'), write(T), write('}"'),nl,
	write('}'),nl,
	fail.
% xtt groups
dotgen_xtt_body:-
	ard_group(Super,Sub),
	write('subgraph "cluster_'),write(Super),write('" {'),nl,
	write('label="'),write(Super),write('"'),nl,
	write('color=blue'),nl,
	write('fontcolor=blue'),nl,
	write('fontsize=8'),nl,
	write('fontname=sans'),nl,
	write('labeljust=l'),nl,
	write('subgraph "cluster_'),write(Sub),write('" {'),nl,
	write('label="'),write(Sub),write('"'),nl,
	write('color=blue'),nl,
	write('fontcolor=blue'),nl,
	write('fontsize=10'),nl,
	write('fontname=sans'),nl,
	write('labeljust=l'),nl,
	write('}'),nl,
	write('}'),nl,
	fail.
% virgin X-links
dotgen_xtt_body:-
	xtt(F1,T1),
	xtt(F2,T2),
	F1 \= F2,
	T1 \= T2,
	member(M,T1), member(M,F2),
	write('"{'), write(F1), write('|'), write(T1), write('}"'), write('->'),
	write('"{'), write(F2), write('|'), write(T2), write('}"'), nl,
	fail.
dotgen_xtt_body.


% helpers to generate xtt groups
xtt_belongs_to_group(F,T,G):-
	ard_att(G),
	forall((
		member(Fmember,F),
		member(Tmember,T),
		member(Fattribute,Fmember),
		member(Tattribute,Tmember)
		),
	       (
		ard_group(G,Fattribute),
		ard_group(G,Tattribute)
	       )
	      ).

ard_group(Group,Attribute):- ard_att(Group),  ard_hist_dep([Group],[Attribute]).

%helper
ard_hist_dep(Pred,Suc):-ard_hist(Pred,Suc).
ard_hist_dep(Pred,Suc):-ard_hist(Pred,X), ard_hist_dep(X,Suc).


