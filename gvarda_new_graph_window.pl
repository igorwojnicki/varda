%
% $Id: gvarda_new_graph_window.pl 99 2009-09-27 12:28:34Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_new_graph_window.pl 99 2009-09-27 12:28:34Z jar0s $'),nl.

:- ensure_loaded('gvarda_utils').

:- pce_begin_class(new_graph_window, dialog).

initialise(F, Ard:ard_graph) :->
	"Creates new instance of new_graph_window."::
	send(F, send_super, initialise, 'Create new graph'),
	new(Lb, list_browser),
	send(Lb, multiple_selection, @on),
	send(Lb, label, 'Attributes:'),

	new(Ti, text_item('New attribute:', '',
		message(F, add_list_item, Lb, @receiver))),
	send(Ti, default, ''),

	send(F, append, Ti),
	send(F, append, button(add,
		message(F, add_list_item, Lb, Ti)), right),
	
	send(F, append, Lb, next_row),
	send(F, append, new(LBB, dialog_group(buttons, group)), right),
	send(LBB, border, size(5, 70)),
	send(LBB, append, button(remove,
		message(F, remove_list_item, Lb))),
	send(LBB, layout_dialog),
	

	send(F, append, button(done,
		message(F, create_graph, Lb)), below),
	send(F, append, button('Cancel',
		message(F?frame, uncreate)), right),
	new(_, partof_hyper(Ard, F, new_graph, ard_graph)),

	send(Ti, keyboard_focus),
	send(F, open).

add_list_item(_, List:list_browser, Txtfield:text_item) :->
	"Adds new attribute from Txtfield to the list."::
	get(Txtfield, selection, Attribute),
	'' \= Attribute,
	send(List, append, dict_item(Txtfield?selection)),
	send(Txtfield, clear).

remove_list_item(_, List:list_browser) :->
	"Removes selected item(s) from the list."::
	send(List?selection, for_all,
		message(List?dict, delete, @arg1)).

create_graph(F, List:list_browser) :->
	"Creates new ARD attributes out of the list and the ARD property based on them."::
	get(List?dict, members, Chain),
	get(Chain, copy, ChainCopy),
	create_list_from_dict(ChainCopy, [], L),
	( ard_property_add(L)
	->	send(?(F, hypered, ard_graph), generate),
		send(F?frame, uncreate)
	;	send(Chain, for_all, message(@prolog, rollback_att_add, @arg1?key)),
		send(@display, report, error, '%s',
				     'Error occured while creating new graph.')
	).

:- pce_end_class.
