%
% $Id: varda.pl,v 1.19 2008-03-13 23:10:04 wojnicki Exp $
%
% Copyright 2007,8 by Grzegorz J. Nalepa and Igor Wojnicki
%
% VARDA the main shell
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


:- write('VARDA: $Id: varda.pl,v 1.19 2008-03-13 23:10:04 wojnicki Exp $'),nl.
:-write('\nCopyright (C) by HeKaTe Project 2006-2009\nVARDA is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\n').

:- ensure_loaded('varda_model').
:- ensure_loaded('varda_view').
:- ensure_loaded('varda_xtt').
:- ensure_loaded('varda_refactor').
:- ensure_loaded('varda_xml').

hlp :- vardahelp.

vardahelp :-
	nl,
	write(' sar.  show the ARD diagram, can be used sar(File),'), nl,
	write(' shi.  show the ARD history (TPH), can be used shi(File),'), nl,
	write(' sha.  show the TPH and ARD combined, with can be used sha(File),'), nl,	
	write(' hic.  ARD history collapse, one level up,'), nl,
	write(' tic.  TPH/ARD history collapse, one level up,'), nl,
	write(' gax.  generate XTT, '), nl,
	write(' sxt.  show the XTT diagram, can be used sxt(File),'), nl,
	write(' pxt.  print the XTT diagram,'), nl,
	write(' xop.  optimize XTT,'), nl,
	write(' kid.  kill all displays,'), nl,
        write(' axg.  write ARD model in XML, can be used axg(File),'), nl,
	write(' xxg.  write XTT model in XTTML2.0, can be used xxg(File),'), nl,
        write(' arp.  write ARD model in Prolog, can be used arp(File),\n       the file can be subsequently consulted to recreate the model,'), nl,
	write(' pur.  purge the entire model.'), nl,
	write(' pux.  purge the XTT model.'), nl,
	nl,
	write(' hlp.  get help,'), nl,
	write(' hlt.  go \"bye, bye!\".'), nl,
	nl,
	varda_model_exists,
	nl.
	
varda_model_exists :-
	ard_depend(_,_),
	ard_property(_),
	ard_hist(_,_),!,
	write('Some Varda model exists.'), nl.
varda_model_exists :-
	write('No Varda model found!'),nl.

hlt :- halt. %the VARDA way of 3letters ;) --GJN

:-
	nl,
	write('the Visual ARD Rapid Development Alloy is READY.'), nl,
	write('Use \'hlp.\' or \'vardahelp.\' to get some help'), nl,
	nl,
%	varda_model_exists, % bad idea if it is loaded from model
	nl.
