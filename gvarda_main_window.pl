%
% $Id: gvarda_main_window.pl 106 2009-09-28 09:11:47Z jar0s $
%
% Copyright 2009 by Maria Miskowiec (miskowiecmaria@gmail.com), Jaroslaw Marek (jar0s@poczta.fm)
%
%     This file is part of GVARDA.
%
%     GVARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GVARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GVARDA.  If not, see <http://www.gnu.org/licenses/>.
%

:- write('GVARDA: $Id: gvarda_main_window.pl 106 2009-09-28 09:11:47Z jar0s $'),nl.

:- use_module(library(pce)).

:- ensure_loaded([
		gvarda_ard_graph
		, gvarda_tph_graph
		, gvarda_new_graph_window
		, gvarda_redo_log
		, gvarda_utils
	]).

:- pce_begin_class(gvarda_main_window, frame).

variable(ard, ard_graph, get, "ARD graph.").
variable(tph, tph_graph, get, "TPH graph.").
variable(undo_props, chain, get, "Chain of properties which were modified. Used for undo operation.").
variable(redo_logs, code_vector, get, "Logs to enable redo operations.").
variable(redo_pointer, int, get, "Points at next possible redo operation.").

initialise(Gv) :->
	"Create main window"::
	send(Gv, send_super, initialise, 'GVARDA'),
	send(Gv, append, new(Content, dialog)),
	send(Content, border, size(2, 2)),
	new(Menu, menu_bar),	
	send(Gv, fill_menu_dialog, Menu),
	send(Content, append, Menu),	
	
	new(Ts, tab_stack),
	send(Ts, append, new(Ts_ard, tab('ARD'))),
	send(Ts, append, new(Ts_tph, tab('TPH'))),

	send(Ts, below, Menu),

	send(Ts_ard, append, new(Ard_picture, ard_graph(Gv, 800, 400))),
	send(Ts_tph, append, new(Tph_picture, tph_graph(800, 400))),	
	send(Gv, slot, ard, Ard_picture),
	send(Gv, slot, tph, Tph_picture),
	send(Gv, slot, undo_props, new(_, chain)),
	send(Gv, slot, redo_logs, new(_, code_vector)),
	send(Gv, slot, redo_pointer, 0),
	send(Ts_tph, recogniser, click_gesture(left, '', single,
				message(Tph_picture, generate))),
	
	send(Content, resize_message, message(Gv, resize_graphs, Ts, Ard_picture, Tph_picture, @arg2)),
	( send(Gv, ard_exists)
	->	send(Ard_picture, generate)
	; true
	),
	send(Gv, open).

:- pce_group(internal).

clear(Gv) :->
	"Clear the diagrams"::
	send(Gv?ard, clear),
	send(Gv?tph, clear).

resize_graphs(_, Ts:tab_stack, Ard:ard_graph, Tph:tph_graph, Size:size) :->
	"Call resize methods due to change of the window size."::	
	send(Ard, size, size(Size?width - 45, Size?height - 82)),
	send(Tph, size, size(Size?width - 45, Size?height - 82)),
	send(Ts, layout_dialog, size(Size?width - 5, Size?height - 33)).

fill_menu_dialog(Gv, MB:menu_bar) :->
	"Fill the menu positions"::
	send(MB, append, new(F, popup(file))),
	send(MB, append, new(E, popup(edit))),
	send(MB, append, new(G, popup(graph))),
	send(MB, append, new(V, popup(view))),
	send(MB, append, new(H, popup(help))),

	send_list(F, append,
		  [ menu_item(load,
			      message(Gv, load))
		  , menu_item('save (Ctrl+S)',
			      message(Gv, save),
				@default, @on,
				message(Gv, ard_exists), '\C-s')
		  , menu_item(postscript,
			      message(Gv, postscript),
				@default, @off,
				message(Gv, ard_exists))
		  %, menu_item('print (Ctrl+P)',
		%	      @nil,
		%		@default, @on,
		%		message(Gv, ard_exists))
		  , menu_item(quit,
			      message(Gv, uncreate),
			      @default, @off)
		  ]),
	send_list(E, append,
		  [ menu_item(undo,
			      message(Gv, undo),
				@default, @off,
				message(Gv, undo_possible))
		  , menu_item(redo,
			      message(Gv, redo),
				@default, @off,
				message(Gv, redo_possible))
		  ]),
	send_list(G, append,
		[ menu_item(new,
			message(Gv, new_ard))
		, menu_item(clear,
			message(Gv, clear_graphs),
			@default, @on,
			message(Gv, ard_exists))
		, new(GM, popup(modify))
		]),
	send_list(GM, append,
		[ menu_item(finalize,
			message(Gv, open_finalize),
			@default, @off,
			and(
			message(Gv, ard_exists),
			message(Gv?ard, has_selected),
			message(Gv?ard?selected_node, can_finalize)
			))
		, menu_item(split,
			message(Gv, open_split),
			@default, @off,
			and(
			message(Gv, ard_exists),
			message(Gv?ard, has_selected),
			message(Gv?ard?selected_node, can_split)
			))
		]),
	send_list(V, append,
		[ menu_item(ard,
			message(Gv, view_ard))
		, menu_item(tph,
			message(Gv, view_tph))
		]),
	send_list(H, append,
		[ menu_item(about,
			message(Gv, about))
		]).

ard_exists(_) :->
	"Test if any ARD nodes exist."::
	ard_property(_).

undo_possible(Gv) :->
	"Test if undo operation is possible."::
	\+ send(Gv?undo_props, empty).

redo_possible(Gv) :->
	"Test if redo operation is possible."::
	get(Gv, redo_pointer, Pointer),
	Pointer > 0,
	get(Gv, redo_logs, Logs),
	get(Logs, size, Size),
	Pointer =< Size.

log_split(Gv, Property:code_vector, NodesList:code_vector, Relations:code_vector) :->
	"Log split operation to undo/redo log.\n All arguments passed into this method are the same as got last ard_split() call. Thus this split operation will be recovered after redo as: ard_aplit(Property, NodesList, Relations)"::
	get(Gv, undo_props, UndoProps),
	send(UndoProps, append, Property),
	send(Gv, slot, undo_props, UndoProps),
	new(Args, code_vector),
	send(Args, append, Property),
	send(Args, append, NodesList),
	send(Args, append, Relations),
	new(Rl, redo_log('split', Args)),
	send(Gv, clear_redo_log),
	get(Gv, redo_logs, RedoLogs),
	send(RedoLogs, append, Rl),
	send(Gv, slot, redo_logs, RedoLogs),
	send(Gv, slot, redo_pointer, RedoLogs?size + 1).

log_finalize(Gv, Property:code_vector, Attributes:code_vector) :->
	"Log finalize operation to undo/redo log \n All arguments passed into this method are the same as got last ard_finalize() call. Thus this finalize operation will be recovered after redo as: ard_finalize(Property, Attributes)"::
	get(Gv, undo_props, UndoProps),
	send(UndoProps, append, Property),
	send(Gv, slot, undo_props, UndoProps),
	new(Args, code_vector),
	send(Args, append, Property),
	send(Args, append, Attributes),
	new(Rl, redo_log('finalize', Args)),
	send(Gv, clear_redo_log),
	get(Gv, redo_logs, RedoLogs),
	send(RedoLogs, append, Rl),
	send(Gv, slot, redo_logs, RedoLogs),
	send(Gv, slot, redo_pointer, RedoLogs?size + 1).

clear_redo_log(Gv) :->
	"Removes all redo logs after current change. \nIt is invoked after each transformation on the graph, preventing the user redoing operations not allowed any more."::
	get(Gv, redo_pointer, Pointer),
	get(Gv, redo_logs, Logs),
	new(NewLogs, code_vector),
	rewrite_logs(Logs, NewLogs, 1, Pointer),
	send(Logs, free),
	send(Gv, slot, redo_logs, NewLogs).

:- pce_group('Menu actions').

%define global holding the file chooser dialog. To be used for save/load operations.
:- pce_autoload(finder, library(find_file)).
:- pce_global(@finder, new(finder)).

postscript(Gv) :->
	"Create PostScript in file"::
	get(@finder, file, save, '.eps', FileName),
	get(Gv, member, dialog, Content),	
	get(Content, member, tab_stack, Ts),
	get(Ts, on_top, Tab),
	get(Tab, graphicals, Graphicals),
	get(Graphicals, head, Pict),
	new(File, file(FileName)),
	send(File, open, write),
	send(File, append, Pict?postscript),
	send(File, close),
	send(File, done).

save(_) :->
	"Save current ARD model to the file."::
	get(@finder, file, save, '.ard', FileName),
	arp(FileName).

load(Gv) :->
	"Load ARD model from the file."::	
	get(@finder, file, open, ['.ard','.*'], FileName),
	send(Gv, clear_graphs),
	consult(FileName),
	send(Gv?ard, generate),
	send(Gv?tph, generate).

new_ard(Gv) :->
	"Clears the knowledge base and open window to create first property of new ARD"::
	send(Gv, clear_graphs),
	get(Gv, ard, Ard),	
	new(_, new_graph_window(Ard)).

clear_graphs(Gv) :->
	"Clear the knowledge base, the ARD and the TPH graphs."::
	retractall(ard_att(_)),
	retractall(ard_property(_)),
	retractall(ard_depend(_, _)),
	retractall(ard_hist(_, _)),
	send(Gv?ard, clear),
	send(Gv?tph, clear).

open_finalize(Gv) :->
	"Open finalize window for the selected node."::
	get(Gv?ard?selected_node, property, Property),	
	new(_, finalize_window(Gv?ard, Property)).

open_split(Gv) :->
	"Open split window for the selected node."::
	get(Gv?ard?selected_node, property, Property),	
	new(_, split_window(Gv?ard, Property)).

view_ard(Gv) :->
	"Switch onto ARD tab."::
	get(Gv, member, dialog, Content),
	get(Content, member, tab_stack, Ts),
	get(Ts, member, 'ARD', Tab),
	send(Ts, on_top, Tab).

view_tph(Gv) :->
	"Switch onto TPH tab."::
	get(Gv, member, dialog, Content),	
	get(Content, member, tab_stack, Ts),
	get(Ts, member, 'TPH', Tab),
	send(Ts, on_top, Tab),
	send(Gv?tph, generate).

about(_) :->
	"Show application info window."::
	send(@display, inform, '%s\n%s\n%s',
		'GVARDA - Graphical/Visual ARD Rapid Development Alloy',
		'Authors:',
		'Maria Miskowiec, Jaroslaw Marek').

undo(Gv) :->
	"Perform undo operation. \nRollbacks the last user operation."::
	get(Gv, undo_props, UndoChain),
	get(UndoChain, tail, Property),
	gvarda_undo(Property),
	send(Gv?ard, generate),
	send(Gv?tph, generate),
	send(UndoChain, delete_tail),
	send(Gv, slot, undo_props, UndoChain),
	get(Gv, redo_pointer, Rp),
	send(Gv, slot, redo_pointer, Rp - 1).

redo(Gv) :->
	"Perform redo operation. \nRepeats last rollbacked user operation."::
	get(Gv, redo_pointer, Pointer),
	get(Gv, redo_logs, Logs),
	get(Logs, element, Pointer, Rl),
	get(Rl, operation, Operation),
	( Operation == 'split'
	-> 	get(Rl, arguments, Args),
		get(Args, element, 1, Property),
		create_list_from_code_vector(Property, PList),
		get(Args, element, 2, Nodes),
		convert_arg(Nodes, NodesList),
		get(Args, element, 3, Relations),
		convert_arg(Relations, RList),
		ard_split(PList, NodesList, RList),
		send(Gv, slot, redo_pointer, Pointer + 1),
		get(Gv, undo_props, UndoProps),
		send(UndoProps, append, Property),
		send(Gv, slot, undo_props, UndoProps)
	; ( Operation == 'finalize'
		-> 
		get(Rl, arguments, Args),
		get(Args, element, 1, Property),
		create_list_from_code_vector(Property, PList),
		get(Args, element, 2, Attributes),
		convert_arg(Attributes, AList),
		send(Attributes, for_all, message(@prolog, add_att_conditional, @arg1)),
		ard_finalize(PList, AList),
		send(Gv, slot, redo_pointer, Pointer + 1),
		get(Gv, undo_props, UndoProps),
		send(UndoProps, append, Property),
		send(Gv, slot, undo_props, UndoProps)
		; 
		send(@display, report, error, '%s',
					     'Requested redo of unknown operation.')
		)
	),
	send(Gv?ard, generate),
	send(Gv?tph, generate).

:- pce_end_class.
