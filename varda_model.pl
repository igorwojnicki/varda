%
% $Id: varda_model.pl,v 1.16 2009-09-29 13:34:02 wojnicki Exp $
%
% Copyright 2007,8 by Grzegorz J. Nalepa and Igor Wojnicki
%
% Basic VARDA ARD model terms and predicates
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


:- write('VARDA: $Id: varda_model.pl,v 1.16 2009-09-29 13:34:02 wojnicki Exp $'),nl.

:-dynamic(ard_att/1).
% ARD attribute name convention: aName, e.g. aTime athermostat
% use: ard_att(aTime).
:-dynamic(ard_property/1).
% ARD property contains a list of attribute names, and is uniquely identified by it
% use: ard_property([aTime]). ard_property([aDate,aHour,aseason]).
% FIXME: list is ordered, whereas semantics is set, but it is just an identifier
:-dynamic(ard_depend/2).
% ARD depend contains exactly two property identifiers, that is attribute lists.
% use: ard_depend([aTime],[aTemperature])

:-dynamic(ard_hist/2).
% ARD history contains property changing history
% i.e. [aTime] property is changed into [time,date] property.
% use: ard_depend([aTime],[time,date])

% display facts describing model
arp :-
	listing(ard_att/1),
	listing(ard_property/1),
	listing(ard_depend/2),
	listing(ard_hist/2),
	fail.
arp.

% store facts describing model in a file F, the file can be consulted in Prolog to recreate the model
arp(F):-
	tell(F), arp, told.


ard_att_add(NewAttribute) :-
	atom(NewAttribute),
	\+ ard_att(NewAttribute),
	assert(ard_att(NewAttribute)).

ard_att_add_list([A]) :-
	ard_att_add(A).
ard_att_add_list([A|Rest]) :-
	ard_att_add(A),
	ard_att_add_list(Rest).

ard_att_del(Attribute) :-
	\+ ard_property_find(Attribute,_),
	retract(ard_att(Attribute)).

ard_property_add(PropertyContents) :-
	\+ ard_property(PropertyContents),
	ard_att_list_verify(PropertyContents),
	assert(ard_property(PropertyContents)).

ard_property_del(PropertyContents) :-
	\+ ard_depend_find(PropertyContents),
	retract(ard_property(PropertyContents)).
% FIXME: maybe we could consider removing orphan attributes, not used in any propertys?
% not really, if we do refactoring - GJN

ard_depend_add(ToProperty,FromProperty) :-
	\+ ard_depend(ToProperty,FromProperty),
	ard_property(ToProperty),
	ard_property(FromProperty),
	assert(ard_depend(ToProperty,FromProperty)).

ard_depend_del(ToProperty,FromProperty) :-
	ard_depend(ToProperty,FromProperty),
	retract(ard_depend(ToProperty,FromProperty)).	

% FIXME: maybe we could consider removing orphan attributes, not used in any propertys?
% not really, if we do refactoring - GJN

%% ard_property_find(+Attribute, -Propertys) is det.
%
%  True, if there are any propertys containing the given attribute.
%
%  @param  Attribute to be looked for
%  @param  List of propertys (identified by attribute names), containing the attribute
%  @author Grzegorz J. Nalepa <gjn@agh.edu.pl>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ard_property_find(Attribute, [Attribute]) :-	
	ard_property([Attribute|_]).
ard_property_find(Attribute, [X|AttributeList]) :-	
	ard_property(X),
	member(Attribute,AttributeList).

%% ard_attlist_verify(+AttributeList) is det.
%
%  True if all of the attribtes in the list exist.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIXME, should verify a set, not list!!!
ard_att_list_verify([SingleAttribute]) :-
	ard_att(SingleAttribute).
ard_att_list_verify([First|ListOfAttributes]) :-
	ard_att(First),
	ard_att_list_verify(ListOfAttributes).
%ard_att_list_verify([]).
ard_att_list_verify(A) :-
	atom(A),
%	write('ERROR: ard_att_list_verify"'), write(A), write('" is an atom, should be a list!'),nl,
% it waz my idea and it makes me sick with hic! --GJN	
	fail.

% FIXME: see exceptions

% FIXME
ard_depend_find(PropertyContents) :-
	ard_depend(PropertyContents,_).
ard_depend_find(PropertyContents) :-
	ard_depend(_,PropertyContents).

% misc utilities
ard_depend_findto(Att,ListOfDepends) :-
	bagof(X,ard_depend(X,Att),ListOfDepends).

ard_depend_findfrom(Att,ListOfDepends) :-
	bagof(Y,ard_depend(Att,Y),ListOfDepends).

% replaces an Element with the Replacement on the list
list_replace(_,_,[],[]).
list_replace(Element,Replacement,[Element|InList],[Replacement|OutList]):-
	list_replace(Element, Replacement, InList,OutList).
list_replace(Element,Replacement,[E|InList],[E|OutList]):-
	Element \= E,
	list_replace(Element, Replacement, InList,OutList).

remove_multiple_deps(ListOfNewAtts,ToDepends2,FromDepends2,ToDepends,FromDepends):-
	member(ListOfNewAtts,ToDepends2),
	member(ListOfNewAtts,FromDepends2),
	subtract(ToDepends2,[ListOfNewAtts],ToDepends),
	FromDepends2=FromDepends.


ard_depend_addto(NewProperty,[ToDepend]) :-
	ard_depend_add(ToDepend,NewProperty).
ard_depend_addto(NewProperty,[First|Depends]) :-
	ard_depend_add(First,NewProperty),	
	ard_depend_addto(NewProperty,Depends).

ard_depend_addfrom(NewProperty,[ToDepend]) :-
	ard_depend_add(NewProperty,ToDepend).
ard_depend_addfrom(NewProperty,[First|Depends]) :-
	ard_depend_add(NewProperty,First),	
	ard_depend_addfrom(NewProperty,Depends).
ard_depend_addfrom(_,[]).

ard_depend_delto(NewProperty,[ToDepend]) :-
	ard_depend_del(ToDepend,NewProperty).
ard_depend_delto(NewProperty,[First|Depends]) :-
	ard_depend_del(First,NewProperty),	
	ard_depend_delto(NewProperty,Depends).

ard_depend_delfrom(NewProperty,[ToDepend]) :-
	ard_depend_del(NewProperty,ToDepend).
ard_depend_delfrom(NewProperty,[First|Depends]) :-
	ard_depend_del(NewProperty,First),	
	ard_depend_delfrom(NewProperty,Depends).


% adding a list of properties
ard_property_list_add([Property]):-
	ard_property_add(Property).
ard_property_list_add([Property|ListOfPropertys]):-
	ard_property_add(Property),
	ard_property_list_add(ListOfPropertys).

% adding a list of dependencies
ard_depend_list_add([From],[To]):-
	ard_depend_add(From,To).
ard_depend_list_add([From|ListOfFrom],[To]):-
	ard_depend_add(From,To),
	ard_depend_list_add(ListOfFrom,[To]).
ard_depend_list_add([From],[To|ListOfTo]):-
	ard_depend_add(From,To),
	ard_depend_list_add([From],ListOfTo).

% adding dependencies from a list
ard_depend_list_add([]).
ard_depend_list_add([[From,To]|DepList]):-
	ard_depend_add(From,To),
	ard_depend_list_add(DepList).

% removes all dependencies on the Property
ard_depend_del_all(Property):-
	retractall(ard_depend(Property,_)),
	retractall(ard_depend(_,Property)).

% checks if the given att is conceptual, that is the name begins with uppercase letter
ard_att_conceptual(Att) :-
	ard_att(Att),
	string_to_list(Att, [First|_]),
	char_type(First,upper).
% checks if the given att is physical
ard_att_physical(Att) :-
	ard_att(Att),
	\+ ard_att_conceptual(Att).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Transformations 
ard_finalize(From,To):-  ard_transform_finalize(From,To).

% finalization with adding appropriate attributes
ard_finalize_att_add(From,To):-
	ard_att_add_list(To),
	ard_finalize(From,To).

%% transform_finalize(+OldProperty, -ListOfNewAtts) is det.
%
%  @param  A simple property id containing attribute to be finalized, e.g. property([aTime]
%  @param  List of attributes finalizing the original one, e.g. [aDate, aHour, aseason, aoperation]
%  @author Grzegorz J. Nalepa <gjn@agh.edu.pl>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fixme? this assumes that new attributes exist! it is a design decision, we expect the user to add them first
ard_transform_finalize(OldProperty, ListOfNewAtts) :-
	[_] = OldProperty, % work only on simple properties
	ard_property(OldProperty),
	ard_property_add(ListOfNewAtts),
	forall(ard_depend(OldProperty,T),
	       (ard_depend_del(OldProperty,T),
		ard_depend_add(ListOfNewAtts,T))),
	forall(ard_depend(F,OldProperty),
	       (ard_depend_del(F,OldProperty),
		ard_depend_add(F,ListOfNewAtts))),
	ard_hist_add(OldProperty,ListOfNewAtts).

% split which defines functional dependencies - all in one :)
% Dependecy list: [[FromProperty,ToProperty],[FromProperty1,ToProperty1]...]
ard_split(Property,NewPropertyList,DependList):-
	% FIXME check if PropertyList has all attribs from Property
	% FIXME check if all propertys from FeaturList can be added
	% FIXME check if dependencies from DependList concern new propertys or neighboring propertys
	ard_property_list_add(NewPropertyList),
	ard_depend_list_add(DependList),
	ard_depend_del_all(Property),
%FIXED4XML	ard_property_del(Property),
	ard_hist_add(Property,NewPropertyList).


% model cleanup 
pur:-
	retractall(ard_att(_)),
	retractall(ard_depend(_,_)),
	retractall(ard_hist(_,_)),
	retractall(ard_property(_)),
	retractall(xtt(_,_)).

% handling history
ard_hist_add(OldProperty,NewProperty):-
	ard_property(NewProperty),
	assert(ard_hist(OldProperty,NewProperty)).
ard_hist_add(_,[]).
ard_hist_add(OldProperty,[NewProperty|PropertyList]):-
	ard_property(NewProperty),
	assert(ard_hist(OldProperty,NewProperty)),
	ard_hist_add(OldProperty,PropertyList).

ard_hist_del(From,To):-
	retract(ard_hist(From,To)).

ard_hist_findfrom(Att,ListOfDepends) :-
	bagof(X,ard_hist(X,Att),ListOfDepends).
ard_hist_findfrom(_,[]).

ard_hist_findto(Att,ListOfDepends) :-
	bagof(Y,ard_hist(Att,Y),ListOfDepends).
ard_hist_findto(_,[]).

% append a new attribute to a TPH node
ard_hist_append(NewAtt,TPHproperty) :-
	ard_att(NewAtt),
%	ard_hist(TPHproperty,_),
	
	ard_hist_findto(TPHproperty,ToList),
	ard_hist_findfrom(TPHproperty,FromList),

	ard_property_add([NewAtt|TPHproperty]), % fixed after xml model update
	ard_hist_addto([NewAtt|TPHproperty],ToList),	
	ard_hist_addfrom([NewAtt|TPHproperty],FromList),

	ard_hist_delto(TPHproperty,ToList),
	ard_hist_delfrom(TPHproperty,FromList),
	ard_property_del(TPHproperty). % fixed after xml model update


ard_hist_addto(NewProperty,[ToDepend]) :-
	%	ard_hist_insert(ToDepend,NewProperty).
	assert(ard_hist(NewProperty,ToDepend)).
ard_hist_addto(NewProperty,[First|Depends]) :-
	%ard_hist_add(First,NewProperty),
	assert(ard_hist(NewProperty,First)),
	ard_hist_addto(NewProperty,Depends).
ard_hist_addto(_,[]).

ard_hist_addfrom(NewProperty,[ToDepend]) :-
	assert(ard_hist(ToDepend,NewProperty)).
ard_hist_addfrom(NewProperty,[First|Depends]) :-
%	ard_hist_add(NewProperty,First),
	assert(ard_hist(NewProperty,First)),
	ard_hist_addfrom(NewProperty,Depends).
ard_hist_addfrom(_,[]).

ard_hist_delto(NewProperty,[ToDepend]) :-
	ard_hist_del(NewProperty,ToDepend).
ard_hist_delto(NewProperty,[First|Depends]) :-
	ard_hist_del(NewProperty,First),	
	ard_hist_delto(NewProperty,Depends).
ard_hist_delto(_,[]).

ard_hist_delfrom(NewProperty,[ToDepend]) :-
	ard_hist_del(ToDepend,NewProperty).
ard_hist_delfrom(NewProperty,[First|Depends]) :-
	ard_hist_del(First,NewProperty),	
	ard_hist_delfrom(NewProperty,Depends).
ard_hist_delfrom(_,[]).

%collapse using histroy by 1 level
ard_hist_collapse:-
	% find all collapsable properties
	findall(Property,(ard_hist_property_to_collapse_to(CollapseTo), ard_hist(CollapseTo,Property)),PropList),
	% collapse these properties with backtracking over each of them (need for the ard_hist_property_collapse/1 collapsing algorithm)
	bagof(X,(member(X,PropList),ard_hist_property_collapse(X)),_).


%collapse property
ard_hist_property_collapse(CollapsedProperty):-
	ard_hist_property_to_collapse_to(Property),
	ard_hist(Property,_),
%	write('add-check:'), write(Property), nl,
%	ard_property_add(Property),
%	write('add-done: '), write(Property), nl,
%	!,
	ard_hist(Property,CollapsedProperty),
%	write(Property),write('<-'),write(CollapsedProperty),nl,
	ard_hist_property_collapse_helper(Property,CollapsedProperty).
ard_hist_property_collapse_helper(NewProperty,CollapsedProperty):-
	ard_depend(From,CollapsedProperty),
	\+ ard_hist(NewProperty,From),
	ard_depend_add(From,NewProperty).
%	write('add: '), write(From), write('->'), write(NewProperty),nl.
ard_hist_property_collapse_helper(NewProperty,CollapsedProperty):-
	ard_depend(CollapsedProperty,To),
	\+ ard_hist(NewProperty,To),
	ard_depend_add(NewProperty,To).
%	write('add: '),write(NewProperty), write('->'), write(To),nl.
ard_hist_property_collapse_helper(F,CF):-
	ard_depend_del_all(CF),
	ard_property_del(CF),
	ard_hist_del(F,CF).
%	write('del: '),write(CF),nl.


% find properties which have predecesoors which don't have predecessors
ard_hist_property_to_collapse_to(Property):-
	ard_hist(Property,_),
	forall(ard_hist(Property,Child),\+ ard_hist(Child,_)).
