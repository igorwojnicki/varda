%
% $Id: varda_view.pl,v 1.6 2008-06-09 09:22:20 gjn Exp $
%
% Copyright 2007,8 by Igor Wojnicki and Grzegorz J. Nalepa
%
% VARDA GraphViz predicates
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


:- write('VARDA: $Id: varda_view.pl,v 1.6 2008-06-09 09:22:20 gjn Exp $'),nl.

% Edinburhg pipe/1 fix, tell(pipe('sth..')) does not work anymore
% still there is a problem with Debian which changes /bin/sh to /bin/dash which doest not work with '&', workaround: reconfigure debian to use /bin/bash as /bin/sh: dpkg-reconfigure dash (do not use dash as /bin/sh)
tell(pipe(P)):-
    open(pipe(P),write,S),
    set_output(S).

kid :-
	write('killing the spawned displays'),
	shell('killall display &').

display:-
	write('displaying: ARD diagram'),nl,
	sar.

%show history
shi:-
	ard_hist(_,_),!,
	tell(pipe('dot -Tpng | display -title ARD_history &')),
	dotgen_hist,
	told.

sha:-
	ard_hist(_,_),!,
	tell(pipe('dot -Tpng | display -title ARD_history_with_ARD &')),
	dotgen_hist_ard,
	told.

sha(Name):-
	ard_hist(_,_),!,
	tell(Name),
	dotgen_hist_ard,
	told.

shi(Name):-
	ard_hist(_,_),!,
%	concat(Name,'.dot',File),
	tell(Name),
	dotgen_hist,
	told.

shi(Title) :-
	ard_hist(_,_),!,
	string_concat('dot -Tpng | display -title TPH_',Title,I),
	string_concat(I, ' &',Invocation),
	tell(pipe(Invocation)),
	dotgen_hist,
	told.

dotgen_hist_ard :-
	dotgen_hist_head,
%	dotgen_hist_body_mark,
	dotgen_hist_body,	
	dotgen_hist_deps,
	dotgen_hist_foot.

dotgen_hist_deps :-
	ard_depend(F,T),
	write('"'),write_list(F),write('"'),
	write('->'),
	write('"'),write_list(T),write('"'),
	write(' [weight=2.0, color=blue]'),
	nl,
	fail.
dotgen_hist_deps.

dotgen_hist :-
	dotgen_hist_head,
	dotgen_hist_body,
	dotgen_hist_foot.

dotgen_hist_head :-
	write('digraph G {'),nl,
	write(' rankdir=TB'),nl,
	write(' node [shape=box, fontsize=9, height=0.1, width=0.05 ]'),nl,
	write(' edge [arrowsize=0.5, len=0.1]'),nl.
dotgen_hist_foot :-
	write('}'),nl.

dotgen_hist_body:-
	ard_hist(F,T),
	write('"'),write_list(F),write('"'),
	write('->'),
	write('"'),write_list(T),write('"'),
	nl,
	fail.
dotgen_hist_body.

dotgen_hist_body_mark:-
	ard_hist(F,T),
	\+ ard_hist(T,_),
	write('"'),write_list(F),write('"'),
	write('->'),
	write('"'),write_list(T),write('"'),
	write(' [style=filled, fillcolor=aquamarine]'),
	nl,
	fail.
dotgen_hist_body_mark:-
	ard_hist(F,T),
%	ard_hist(T,_),
%	\+ X = T,
	write('"'),write_list(F),write('"'),
	write('->'),
	write('"'),write_list(T),write('"'),
	nl,
	fail.
dotgen_hist_body_mark.

write_list([]):- !.
write_list([H|T]):-
	write(H), write('\\n'),
	write_list(T),!.


% show ard
sar:-
% must be commented out, to make possible displaying an ard with no dependencies
%	ard_depend(_,_),
%	ard_property(_),
	tell(pipe('dot -Tpng | display -title ARD &')),
	dotgen_ard,
	told.

sar(Name) :-
%	ard_depend(_,_),
%	ard_property(_),
%	concat(Name,'.dot',File),
	tell(Name),
	dotgen_ard,
	told.

sar(Title) :-
% must be commented out, to make possible displaying an ard with no dependencies
%	ard_depend(_,_),
%	ard_property(_),
	string_concat('dot -Tpng | display -title ARD_',Title,I),
	string_concat(I, ' &',Invocation),
	tell(pipe(Invocation)),
%	tell(pipe('dot -Tpng | display -title ARD &')),
	dotgen_ard,
	told.

% make pdf, fig, using dot and fig2dev

dotgen_ard:-
	dotgen_ard_head,
	dotgen_ard_body,
	dotgen_ard_foot.

dotgen_ard_head :-
	write('digraph G {'),nl,
	write(' rankdir=LR'),nl,
	write(' node [shape=box, fontsize=9, height=0.1, width=0.05 ]'),nl,
	write(' edge [arrowsize=0.5, len=0.1]'),nl.
dotgen_ard_foot:-
	write('}'),nl.

dotgen_ard_body:-
 	ard_property(P), \+ ard_hist(P,_),
 	write('"'),write_list(P),write('"'),nl,fail.
%make an orphan property red (it's not in TPH)
dotgen_ard_body:-
 	ard_property(P),
	\+ ard_hist(P,_),
	\+ ard_hist(_,P),
 	write('"'),write_list(P),write('"'),
	write(' [style=filled, fillcolor=red]'),nl,fail.
dotgen_ard_body:-
	ard_depend(F,T),
	write('"'),write_list(F),write('"'),
	write('->'),
	write('"'),write_list(T),write('"'),
	nl,
	fail.
dotgen_ard_body.

	
history_collapse:-
	ard_hist_collapse,
	write('Collapsing by one level...'),nl,
	display,
	stop_key,
	history_collapse.

hic :- history_collapse.

tic :- tph_collapse.

tph_collapse:-
	ard_hist_collapse,
	write('Collapsing by one level, showing TPH...'),nl,
	sha,
	stop_key,
	tph_collapse.
tph_collapse :-
	write('That\'s about it!').

	
stop_key:-
	write(' <Press ENTER to continue, any other to cancel>'),nl,
	get_single_char(13).

