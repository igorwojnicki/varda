%
% $Id: varda_refactor.pl,v 1.7 2008-03-02 23:14:55 gjn Exp $
%
% Copyright 2007,8 by Grzegorz J. Nalepa and Igor Wojnicki
%
% VARDA model transformation predicates
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


:- write('VARDA: $Id: varda_refactor.pl,v 1.7 2008-03-02 23:14:55 gjn Exp $'),nl.


% This predicates adds a new (but existing) attribute to the TPH,
% a list of TPH nodes in which the attribute is to be inserted to is given
% after the last give node, a split is performed,
% and a simple ARD property that holds the new node is created.

% example: trace, ard_att_add('Alpha'),ard_refactor_attribute_add('Alpha',[['D']]).
% example: trace, ard_att_add('alpha'),ard_refactor_attribute_add('alpha',[['D']]).
% accpets a single attribute, and in this case a single element list of nodes, where the att should be added
% it means, that the new att should be 
ard_refactor_attribute_add(NewAtt, [Node]) :-
	ard_att(NewAtt),
%%	write(Node),
	\+ Node = [_], % the node should not be simple, becasue we would not be able to split it
	ard_refactor_propagate_parents(NewAtt,Node),
	ard_hist_append(NewAtt, Node),
%	ard_property_add([NewAtt]),
%	[C] = Node,
	% make a split of a new node
	assert(ard_hist([NewAtt|Node],[NewAtt])),
	ard_property_add([NewAtt]). % atfer xmlmodel fix
%	ard_refactor_update_ard([NewAtt]).
%%nope	ard_property_add([Node]),	
%%	assert(ard_hist([NewAtt|Node],Node)).
%%%
% example: trace, ard_att_add('Beta'),ard_refactor_attribute_add('Be',[['B','C'],['B'],['b']]).
% example: trace, ard_att_add('beta'),ard_refactor_attribute_add('beta',[['B','C'],['B'],['b']]).
% accpets a single attribute, and list of nodes, where the att should be added
ard_refactor_attribute_add(NewAtt, [Node|Specification]) :-
	ard_att(NewAtt),
	write('WARNING: \'refactor_attribute_add\' with a list of TPH nodes can CORRUPT the TPH!'), nl,
	ard_refactor_propagate_parents(NewAtt,Node),
	ard_hist_append(NewAtt, Node),
	ard_hist_append_to_nodes(NewAtt,Specification).
%	ard_hist_(NewAtt,Specification).

ard_hist_append_to_nodes(NewAtt,[LasTPHNode]) :-
%%	ard_property(LasTPHNode),
%	\+ [_] = LasTPHNode, % we should not append to simple nodes
%	\+ ard_hist_is_simpleconceptual(LasTPHNode),
	ard_hist_append(NewAtt,LasTPHNode),
%nope	ard_property_add([NewAtt]),
	assert(ard_hist([NewAtt|LasTPHNode],[NewAtt])), % split the new att from node
%nope	ard_property_add([LasTPHNode]),
%	assert(ard_hist([NewAtt|LasTPHNode],LasTPHNode)),
	ard_property_add([NewAtt]). % atfer xmlmodel fix
%%	ard_refactor_update_ard([NewAtt]).
ard_hist_append_to_nodes(NewAtt, [First|TPHnodeList]) :-
%	ard_property(First),
%	\+ [_] = First, % we should not append to simple nodes
%	\+ ard_hist_is_simpleconceptual(First),
	ard_hist_append(NewAtt,First),
	ard_hist_append_to_nodes(NewAtt,TPHnodeList).

ard_refactor_update_ard([NewAtt]) :-
	ard_att_conceptual(NewAtt). % do nothing, the designer need to handle it
ard_refactor_update_ard([NewAtt]) :-
	ard_att_physical(NewAtt), % we add a simple physical property
	ard_property_add([NewAtt]). % the designer needs to add dependencies manually
ard_refactor_update_ard(_). % we can't work on complex nodes

ard_hist_is_simpleconceptual(Node) :-
	Node = [Att],
	ard_att_conceptual(Att).
	
ard_hist_parent_is_simpleconceptual(Node) :-
	ard_hist([Parent],Node),	% gives the parent of the given node, if parent is simple node
	ard_att_conceptual(Parent).

% do not append att to simpleconcpetual nodes, they are splitted
ard_refactor_propagate_parents(_,Node) :-
	ard_hist(Parent,Node),	% gives the parent of the given node
	ard_hist_is_simpleconceptual(Parent).
%	ard_hist_parent_is_simpleconceptual(Node).
ard_refactor_propagate_parents(NewAtt,Node) :-
	ard_hist(Parent,Node),
	\+ ard_hist_is_simpleconceptual(Parent),
	ard_hist_append(NewAtt, Parent),
	ard_refactor_propagate_parents(NewAtt,[NewAtt|Parent]).

% adds a finalization, from given simple conceptual Node, to given NewNode
ard_refactor_finalize_add(Node, NewNode) :-
	ard_hist(_,Node),
% atfer xmlmodel fix
%	\+ ard_property(Node), % this predicate should not be used on the properties, where you simply use ard_finalize, but on the tph only!
%	Node=[_],%simple node, we should also check if conceptual att
	ard_hist_is_simpleconceptual(Node),
	ard_att_list_verify(NewNode),
	assert(ard_hist(Node,NewNode)),
	ard_property_add(NewNode). % atfer xmlmodel fix
%	ard_refactor_update_ard(NewNode).

% adds a split, from given complex node, to given set of nodes
ard_refactor_split_add(TPHproperty,[NewNode]) :-
	assert(ard_hist(TPHproperty,NewNode)),
%	ard_refactor_update_ard(NewNode).
	ard_property_add(NewNode). % atfer xmlmodel fix
ard_refactor_split_add(TPHproperty,[FirstAtt|NewAtts]) :-
	assert(ard_hist(TPHproperty,FirstAtt)),
	ard_property_add(FirstAtt), % atfer xmlmodel fix
%	ard_refactor_update_ard(FirstAtt),
	ard_refactor_split_add(TPHproperty,NewAtts).

% add a finalization, from given simple conceptual Node,
% to given last node, holding a physical attribute
%ard_refactor_finalize_add_final(Node, [FinalAtt]) :-
%	ard_att_physical(FinalAtt),
%	ard_refactor_finalize_add(Node, [FinalAtt]),


%%%%%%%%%%%%%%%%%%%%%%%%% old shit

% add a new attribute to an existing property at some level
% the ide is to point to a conceptual attribute (in the TPH)
% constituing a simple propertry,
% which should have been splitted to include this new attribute
refactor_finalize_trivial(NewAtt, Attribute, Dependant) :-
	ard_property(Dependant),
	ard_hist(Attribute,_),
	ard_att_add(NewAtt),
	ard_property_add([NewAtt]),
	ard_depend_add([NewAtt],Dependant),
	ard_hist_add(Attribute,[NewAtt]).

refactor_finalize(NewAtt, [[NewAtt|Last],[NewAtt]]) :-
	ard_property_add([NewAtt]),
	ard_hist_findfrom(Last,ToList),
	assert(ard_hist([NewAtt|Last],[NewAtt])),
	ard_hist_addfrom([NewAtt|Last],ToList),	
	ard_hist_delfrom(Last,ToList).
refactor_finalize(NewAtt, [TPHproperty]) :-
	ard_hist_append(NewAtt,TPHproperty).
refactor_finalize(NewAtt, [First|TPHpropertyList]) :-
	ard_hist_append(NewAtt,First),
	refactor_finalize(NewAtt,TPHpropertyList).
% FIXME: this lacks one important feature:
% the introduced attribute should be propagated up in the TPH,
% through splits, to the nearest possible finalization

% testdata frm userv
%	trace,ard_att_add('Client'),refactor_finalize('Client',[['Policy','Premium'],['Policy']]).
%	trace,ard_att_add('Client'),refactor_finalize('Client',[['Policy','Premium'],['Client','Policy'],['Client']]).
%	trace,ard_att_add('Client'),refactor_finalize('Client',[['Client','Policy'],['Client']]).

refactor_finalize_add(Node, NewNode) :-
	ard_hist(_,Node),
	Node=[_],%simple node, we should also check if conceptual att
	assert(ard_hist(Node,NewNode)).
	      
refactor_split([NewAtt],TPHproperty) :-
	assert(ard_hist(TPHproperty,[NewAtt])).
refactor_split([FirstAtt|NewAtts],TPHproperty) :-
	assert(ard_hist(TPHproperty,FirstAtt)),
	refactor_split(NewAtts,TPHproperty).

refactor_split_final([NewAtt],TPHproperty) :-
	assert(ard_hist(TPHproperty,[NewAtt])),
	ard_property_add([NewAtt]).
refactor_split_final([FirstAtt|NewAtts],TPHproperty) :-
	assert(ard_hist(TPHproperty,[FirstAtt])),
	ard_property_add([FirstAtt]),
	refactor_split_final(NewAtts,TPHproperty).

%test
% trace,ard_att_add('C1'),ard_att_add('C2'),ard_att_add('C3'),refactor_split(['C1','C2','C3'],['Car']).
% trace,ard_att_add('C1'),ard_att_add('C2'),ard_att_add('C3'),refactor_split([['C1','C2'],'C3'],['Car']).

% broken
%	trace,ard_att_add('Client'),refactor_finalize('Client',['Policy','Premium'],[['Policy'],['Client']]).
%	trace,ard_att_add('Client'),refactor_finalize('Client',['Policy','Premium'],[['Policy']]).

% (TPH node,list of nodes).
%refactor_finalize_helper(TPHproperty, [PropSingle]).
%refactor_finalize_helper(PropList).
% add a new attribute, to an existing TPH property
%refactor_property(NewAtt, Property).
	

	
