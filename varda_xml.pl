%
% $Id: varda_xml.pl,v 1.11 2009-02-09 20:04:07 wojnicki Exp $
%
% Copyright 2007,8 by Grzegorz J. Nalepa
%
% VARDA XML support
%
%
%     Copyright (C) 2006-9 by the HeKatE Project
%
%     VARDA has been develped by the HeKatE Project, 
%     see http://hekate.ia.agh.edu.pl
%
%     This file is part of VARDA.
%
%     VARDA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     VARDA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with VARDA.  If not, see <http://www.gnu.org/licenses/>.
%


% TODO
% - it is more on less a 3hr ugly hack :>
% - the generator sholud be a context/stack-based, to be able to close_open_tab
% - then it could be SWI xml term compliant

:- write('VARDA: $Id: varda_xml.pl,v 1.11 2009-02-09 20:04:07 wojnicki Exp $'),nl.

:- dynamic(ard_aid/2).
:- dynamic(ard_pid/2).
:- dynamic(xtt_tid/3).

axg :- ard_xml_gen.
axg(F) :- ard_xml_gen(F).

xxg :- xtt_xml_gen.
xxg(F) :- xtt_xml_gen(F).

% ARD XML handling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ard_xml_gen(F) :-
	tell(F),
	ard_xml_gen,
	told.

ard_xml_gen :-
	xml_indent_mark_set('ard'),
	xml_indent_clear('ard'),
	ard_xml_hml_head,
	xml_indent_more('ard'),
	ard_xml_typeset_gen,
	ard_xml_attset_gen,
	ard_xml_propset_gen,
	ard_xml_tph_gen,
	ard_xml_ard_gen,
	xml_indent_less('ard'),
	ard_xml_hml_tail,!.

ard_xml_hml_head :-
	write('<?xml version="1.0" encoding="utf-8" standalone="no"?>'), nl,
	write('<!DOCTYPE hml SYSTEM "hml.dtd">'),nl,
	write('<hml>'),
	nl.

ard_xml_hml_tail :-
	write('</hml>').

ard_xml_typeset_gen :-
	xml_tag_open_indent('type_set',[]), nl,
	xml_tag_close_indent('type_set').

ard_xml_attset_gen :-
	xml_tag_open_indent('attribute_set',[]), nl,
	xml_indent_more('ard'),
	ard_xml_att_gen,
	xml_indent_less('ard'),
	xml_tag_close_indent('attribute_set').

ard_xml_att_gen :-
	ard_att(P),
	ard_attribute_aid(P,NId),
	aid_to_att(NId,Id),
	Atts = [['name',P],['id',Id],['abbrev',P],['value','single'],['class','ro']],
% value="single" class="ro"	
	xml_tag_single_indent('att',Atts),
% 	% to be extended with att limits in future!
	fail.
ard_xml_att_gen.

ard_xml_propset_gen :-
	xml_tag_open_indent('property_set',[]), nl,
	xml_indent_more('ard'),
	ard_xml_prop_gen,
	xml_indent_less('ard'),
	xml_tag_close_indent('property_set').

ard_xml_prop_gen :-
	ard_property(P),
	ard_property_pid(P,NId),
	pid_to_prp(NId,Id),
	Atts = [['id',Id]],
 	xml_tag_open_indent('property',Atts), nl,
	xml_indent_more('ard'),
	ard_xml_propcontents_gen(P),
	xml_indent_less('ard'),
 	xml_tag_close_indent('property'),
	fail.
ard_xml_prop_gen.

ard_xml_propcontents_gen([P1|P]):-
	ard_aid(P1,AId),
	aid_to_att(AId,Id),
	Atts = [['ref',Id]],
	xml_tag_single_indent('attref',Atts),
	ard_xml_propcontents_gen(P).
ard_xml_propcontents_gen([]).

ard_attribute_aid(Prop,Id) :-
	ard_aid(Prop,Id).
ard_attribute_aid(Prop,Id) :-
	\+ ard_aid(Prop,Id),
	ard_attribute_aid_next(Id),
	asserta(ard_aid(Prop,Id)).

ard_attribute_aid_next(Id) :-
	ard_aid(_,Current),
	Id is Current + 1,
	\+ ard_aid(_,Id).
ard_attribute_aid_next(0) :-
	\+ ard_aid(_,_).

aid_to_att(NId,Id) :-
	number_chars(NId,CId),
	string_to_list(Id,['a','t','t','_'|CId]).

ard_property_pid(Prop,Id) :-
	ard_pid(Prop,Id).
ard_property_pid(Prop,Id) :-
	\+ ard_pid(Prop,Id),
	ard_property_pid_next(Id),
	asserta(ard_pid(Prop,Id)).

ard_property_pid_next(Id) :-
	ard_pid(_,Current),
	Id is Current + 1,
	\+ ard_pid(_,Id).
ard_property_pid_next(0) :-
	\+ ard_pid(_,_).

pid_to_prp(NId,Id) :-
	number_chars(NId,CId),
	string_to_list(Id,['p','r','p','_'|CId]).

ard_xml_tph_gen :-
	xml_tag_open_indent('tph',[]), nl,
	xml_indent_more('ard'),
	ard_xml_trans_gen,
	xml_indent_less('ard'),
	xml_tag_close_indent('tph').

ard_xml_trans_gen :-
	ard_hist(P1,P2), %write(P1),write(P2),
	ard_pid(P1,NPid1),
	pid_to_prp(NPid1,Pid1),
	ard_pid(P2,NPid2),
	pid_to_prp(NPid2,Pid2),	
	A1 = [['dst',Pid2]],
	A2 = [['src',Pid1]|A1],
	xml_tag_single_indent('trans',A2),
	fail.
ard_xml_trans_gen.

ard_xml_ard_gen :-
	xml_tag_open_indent('ard',[]), nl,
	xml_indent_more('ard'),
	ard_xml_dep_gen,
	xml_indent_less('ard'),
	xml_tag_close_indent('ard').

ard_xml_dep_gen :-
	ard_depend(P1,P2), 
	ard_pid(P1, NPid1),
	pid_to_prp(NPid1,Pid1),
	ard_pid(P2, NPid2),
	pid_to_prp(NPid2,Pid2),
	A1 = [['dependent',Pid2]],
	A2 = [['independent',Pid1]|A1],
	xml_tag_single_indent('dep',A2),
	fail.
ard_xml_dep_gen.


% XTT XML handling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xtt_xml_gen(F) :-
	tell(F),
	xtt_xml_gen,
	told.

xtt_xml_gen :-
	xml_location_clear,
	xml_indent_mark_set('xtt'),
	xml_indent_clear('xtt'),
	xtt_xml_hml_head,
	xml_indent_more('xtt'),
	xtt_xml_group_gen,
	xtt_xml_attset_gen,
	xtt_xml_tableset_gen,
	xtt_xml_connectionset_gen,
	xml_indent_less('xtt'),
	xtt_xml_hml_tail,!.

xml_location_clear :-
	retractall(xtt_location_y(_)),
	asserta(xtt_location_y(0)),
	retractall(xtt_location_x(_)),
	asserta(xtt_location_x(0)).

xtt_xml_hml_head :-
%	write('<?xml version="1.0" encoding="utf-8"?>'), nl,
	write('<XTTML version="2.0">'), nl.
%	write('<group_list> </group_list>'), nl.

xtt_xml_group_gen :-
	write('<group_list><group name="default" parent=""></group></group_list>').

xtt_xml_hml_tail :-
	write('</XTTML>').

xtt_xml_attset_gen :-
	xml_tag_open_indent('attribute_list',_),nl,
	xml_indent_more('xtt'),
	xtt_xml_att_gen,
	xml_indent_less('xtt'),
	xml_tag_close_indent('attribute_list').

xtt_xml_att_gen :-
	ard_att(P),
	ard_att_physical(P),
%	Atts = [['name',P],['acronym',P],['group',''],['realtions','middle'],['type','integer'],['defvalue','0'],['currvalue','0'],['constraint','no']],
	Atts = [['name',P],['acronym',P],['realtions','middle'],['type','integer']],	
% 	xml_tag_open('att',Atts,To),
	xml_tag_open_indent('xtt_attribute',Atts),nl,
% 	% to be extended with att limits in future!
	xml_tag_close_indent('xtt_attribute'),
	fail.
xtt_xml_att_gen.

xtt_xml_tableset_gen :-
	xml_tag_open_indent('xtt_list_table',_),nl,
	xml_indent_more('xtt'),
	xtt_xml_table_gen,
	xml_indent_less('xtt'),
	xml_tag_close_indent('xtt_list_table').

xtt_xml_table_gen :-
	xtt(C,D),
	xtt_table_tid(C,D,Id),
	Atts = [['name',Id],['table_type','middle']],
 	xml_tag_open_indent('xtt_table',Atts),nl,
	xml_indent_more('xtt'),
	xtt_xml_table_row(C,D),
	xtt_xml_table_context(C,D),
	xtt_xml_table_columns(C,D),
	xtt_xml_table_location(C,D),
	xml_indent_less('xtt'),
 	xml_tag_close_indent('xtt_table'),
	fail.
xtt_xml_table_gen.

xtt_xml_table_context(C,D) :-
	length(C,LC),
	length(D,LD),
	Atts = [['conditional',LC],['assert','0'],['retract','0'],['action',LD]],
 	xml_tag_single_indent('context_size',Atts).

xtt_xml_table_columns(C,D) :-
 	xml_tag_open_indent('column_attributes',[]),nl,
	xml_indent_more('xtt'),
	xtt_xml_column_helper(C,D),
	xml_indent_less('xtt'),
 	xml_tag_close_indent('column_attributes').

xtt_xml_column_helper([],[]).
xtt_xml_column_helper([],[D1|D]) :-
 	xml_tag_open_indent('ca',[]),
	D1=[D2],
	write(D2),
 	xml_tag_close_noindent('ca'),
	xtt_xml_column_helper([],D).
xtt_xml_column_helper([C1|C],D) :-
 	xml_tag_open_indent('ca',[]),
	C1=[C2],
	write(C2),
 	xml_tag_close_noindent('ca'),
	xtt_xml_column_helper(C,D).

:- dynamic(xtt_location/1).

xtt_xml_table_location(_,_) :-
	xtt_location_y(Y),
	xtt_location_x(X),
%	xtt_xml_table_location_x(C,D,X2),
%	X is X1 + 10,		%X2,
	A=[['xpos',X],['ypos',Y]],
 	xml_tag_open_indent('localization',A),nl,
 	xml_tag_close_indent('localization'),
	Y1 is Y + 50, % fixme: ugly hack
	X3 is X + 30,
	retractall(xtt_location_y(_)),
	asserta(xtt_location_y(Y1)),
	retractall(xtt_location_x(_)),
	asserta(xtt_location_x(X3)).

xtt_xml_table_location_x(C,D,0) :-
	xtt_table_tid(C,D,I),
	\+ xtt_xml_connection_get(_,I).
xtt_xml_table_location_x(C,D,300) :-
	xtt_table_tid(C,D,I),
	\+ xtt_xml_connection_get(I,_).
xtt_xml_table_location_x(C,D,150) :- 
	xtt_table_tid(C,D,I),
	xtt_xml_connection_get(_,I),
	xtt_xml_connection_get(I,_).

xtt_xml_table_row(C,D) :-
 	xml_tag_open_indent('row',[]),nl,
	xml_indent_more('xtt'),
	xtt_xml_row_helper(C,D),
	xml_indent_less('xtt'),
 	xml_tag_close_indent('row').

xtt_xml_row_helper([],[]).
xtt_xml_row_helper([],[_|D]) :-
	A = [[operator,'e'],[content,'op:nd']],
 	xml_tag_single_indent('cell',A),
	xtt_xml_row_helper([],D).
xtt_xml_row_helper([_|C],D) :-
	A = [[operator,'e'],[content,'op:any']],
 	xml_tag_single_indent('cell',A),
	xtt_xml_row_helper(C,D).

xtt_table_tid(C,D,Id) :-
	xtt_tid(C,D,Id).
xtt_table_tid(C,D,Id) :-
	\+ xtt_tid(C,D,Id),
	xtt_table_tid_next(Id),
	asserta(xtt_tid(C,D,Id)).

xtt_table_tid_next(Id) :-
	xtt_tid(_,_,Current),
	Id is Current + 1,
	\+ xtt_tid(_,_,Id).
xtt_table_tid_next(Id) :-
	\+ xtt_tid(_,_,_),
	Id = 1.

xtt_xml_connectionset_gen :-
	xtt_xml_connection_get(_,_),
	xml_tag_open_indent('xtt_list_connections',_),nl,
	xtt_xml_connection_gen,
	xml_tag_close_indent('xtt_list_connections').
xtt_xml_connectionset_gen.

xtt_xml_connection_get(F,T) :-
	xtt(A,B),
	xtt(C,D),
	A\=C,
	B\=D,
	member(M,B),
	member(M,C),
	xtt_table_tid(A,B,F),
	xtt_table_tid(C,D,T).

xtt_xml_connection_gen :-
	xtt_xml_connection_get(F,T),
	Atts = [['cut','false'],['source_table_name',F],['destination_table_name',T],['source_row','0'],['destination_row','0']],
 	xml_tag_open_indent('xtt_connection',Atts),nl,
	xml_tag_close_indent('xtt_connection'),
	fail.
xtt_xml_connection_gen.


% General XML handling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xml_tag_single(Tag,Atts,Out) :-
	var(Atts),
	concat_atom(['<',Tag,'>'],Out).
xml_tag_single(Tag,Atts,Out) :-
	xml_atts_process(Atts,AttsProc),
	concat_atom(['<',Tag,AttsProc,'/>'],Out).
xml_tag_open(Tag,Atts,Out) :-
	var(Atts),
	concat_atom(['<',Tag,'>'],Out).
xml_tag_open(Tag,Atts,Out) :-
	xml_atts_process(Atts,AttsProc),
	concat_atom(['<',Tag,AttsProc,'>'],Out).
xml_tag_close(Tag,Out) :-
	concat_atom(['</',Tag,'>'],Out).

xml_tag_open_indent(Tag,Atts) :-
	xml_indent_current(CI),
	xml_indent(CI,L),
	xml_tag_open(Tag,Atts,To),
	concat(L,To,TagOpen),
	write(TagOpen).

xml_tag_single_indent(Tag,Atts) :-
	xml_indent_current(CI),
	xml_indent(CI,L),
	xml_tag_single(Tag,Atts,To),
	concat(L,To,TagOpen),
	write(TagOpen),
	nl.

xml_tag_close_indent(Tag) :-
	xml_indent_current(CI),
	xml_indent(CI,L),
	xml_tag_close(Tag,To),
	concat(L,To,TagOpen),
	write(TagOpen),
	nl.

xml_tag_close_noindent(Tag) :-
	xml_tag_close(Tag,To),
	write(To),
	nl.

xml_content(C) :-
	xml_indent_current(CI),
	xml_indent(CI,L),
	concat(L,C,TagOpen),
	write(TagOpen),
	nl.

% list of pairs, name,value
xml_atts_process([],'').
xml_atts_process([A1|A],P) :-
	A1=[N,V],
	xml_att_build(N,V,P1),
	xml_atts_process(A,P2),
	concat(P1,P2,P).
xml_att_build(N,V,P) :-
	concat_atom([' ',N,'=','"',V,'"'],P).

:-dynamic(xml_indent/2).
:-dynamic(xml_indent_current/1).

xml_indent_clear(Mark) :-
	retractall(xml_indent(Mark,_)),
	asserta(xml_indent(Mark,'')).
xml_indent_more(Mark) :-
	xml_indent(Mark,''),
	retractall(xml_indent(Mark,_)),	
	asserta(xml_indent(Mark,'    ')).
xml_indent_more(Mark) :-
	xml_indent(Mark,L),
	concat('    ',L,N),
	retractall(xml_indent(Mark,_)),
	asserta(xml_indent(Mark,N)).
xml_indent_less(Mark) :-
	xml_indent(Mark,'').
xml_indent_less(Mark) :-
	xml_indent(Mark,L),
	concat('    ',N,L),
	retractall(xml_indent(Mark,_)),
	asserta(xml_indent(Mark,N)).
	
xml_indent_mark_set(M) :-
	retractall(xml_indent_current(_)),
	asserta(xml_indent_current(M)).
